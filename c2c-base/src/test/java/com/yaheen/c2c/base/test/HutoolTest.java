package com.yaheen.c2c.base.test;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NetUtil;
import cn.hutool.core.util.RandomUtil;
import org.junit.Test;

/**
 * FileName: HutoolTest
 * Author:   Honsun
 * Date:     2019/3/21 16:17
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 16:17     2.0.0
 */
public class HutoolTest {

    @Test
    //有问题，不能使用
    public void testIdUtil(){
        long startTime=System.currentTimeMillis();
        for(int i=0;i<100;i++){
            System.out.println(IdUtil.createSnowflake(0,0).nextId());
        }
        System.out.println("hutool consume:"+ (System.currentTimeMillis()-startTime));
    }

    @Test
    //可以用于生成appSecret
    public void testRandomUtil(){
        for(int i=0;i<100;i++){
            System.out.println(RandomUtil.randomString(18).toUpperCase());
        }
    }

    @Test
    public void testNetUtil(){
        String ip="198.161.100.88";
        System.out.println(NetUtil.ipv4ToLong(ip));
        System.out.println(NetUtil.longToIpv4(3332465752L));
    }

    @Test
    public void testFileUtil(){
        String file="nicholas.jpg";
        System.out.println(FileUtil.mainName(file));
        System.out.println(FileUtil.extName(file));
    }
}
