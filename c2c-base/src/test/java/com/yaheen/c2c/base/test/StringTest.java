package com.yaheen.c2c.base.test;

/**
 * FileName: StringTest
 * Author:   Honsun
 * Date:     2019/4/1 16:02
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/4/1 16:02     2.0.0
 */
public class StringTest {

    public static void main(String[] args) {
        Integer a=1;
        Integer b=1;
        System.out.println(a.equals(b));
    }
}
