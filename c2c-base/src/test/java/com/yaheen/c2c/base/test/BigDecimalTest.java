package com.yaheen.c2c.base.test;

import java.math.BigDecimal;

/**
 * FileName: BigDecimalTest
 * Author:   Honsun
 * Date:     2019/3/28 15:39
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/28 15:39     2.0.0
 */
public class BigDecimalTest {

    public static void main(String[] args) {
//        String charge="0.015";
//        BigDecimal db=new BigDecimal(charge);
//        //取4位，四舍五入
//        db=db.setScale(4, BigDecimal.ROUND_HALF_UP);
//        System.out.println(db);
//        BigDecimal db2=new BigDecimal("1.0");
//        db2=db2.setScale(4, BigDecimal.ROUND_HALF_UP);
//        BigDecimal db3=db2.subtract(db);
//        System.out.println(db3.multiply(new BigDecimal("50")));
        String money="0.000026";
        System.out.println(new BigDecimal(money).setScale(4, BigDecimal.ROUND_DOWN));
    }
}
