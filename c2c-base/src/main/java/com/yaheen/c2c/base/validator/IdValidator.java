package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;

/**
 * FileName: IdValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 特定ID校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class IdValidator extends ValidatorHandler<Object> implements Validator<Object> {

    /**
     * 字段名称
     **/
    private String fieldName;

    public IdValidator(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Object s) {
        if (s == null) {
            context.addError(ValidationError.create(String.format("%s不能为空", fieldName)));
            return false;
        }
        if (!(s instanceof String)) {
            context.addError(ValidationError.create(String.format("%s必须是字符串", fieldName)));
            return false;
        }
        String value = s.toString();
        if (value.length() != 18  || !value.matches("[0-9]+")) {
            context.addError(ValidationError.create(String.format("%s必须是18位纯数字的字符串", fieldName)));
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        String id = "411115162062290944";
        boolean result = id.matches("[0-9]+");
        if (result == true) {
            System.out.println("该字符串是纯数字");
        } else {
            System.out.println("该字符串不是纯数字");
        }
    }
}
