package com.yaheen.c2c.base.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import javax.servlet.http.HttpServletResponse;

/**
 * FileName: OriginInteceptor
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 允许跨域访问
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class OriginInteceptor implements Interceptor {

    @Override
    public void intercept(Invocation invocation) {
        Controller controller = invocation.getController();
        //设置允许跨域的参数
        HttpServletResponse response = controller.getResponse();
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "Accept,Origin,X-Requested-With,Content-Type,Last-Modified,platform_id,user_id");
        response.setHeader("Content-Type", "application/json;charset=utf-8");
        response.setHeader("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        invocation.invoke();
    }
}
