package com.yaheen.c2c.base.support;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.jfinal.kit.StrKit;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

/**
 * FileName: Decrypt
 * Author:   Honsun
 * Date:     2019/4/16 10:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/4/16 10:43     2.0.0             AES加密解密
 */
public class Decrypt {

    /**
     * 加密
     * 1.构造密钥生成器
     * 2.根据ecnodeRules规则初始化密钥生成器
     * 3.产生密钥
     * 4.创建和初始化密码器
     * 5.内容加密
     * 6.返回字符串
     */
    public static String AESEncode(String content, String encodeRules) {
        try {
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keygen = KeyGenerator.getInstance(SymmetricAlgorithm.AES.getValue());
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(encodeRules.getBytes());
            keygen.init(128, random);
            //3.产生原始对称密钥
            SecretKey originalKey = keygen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte[] raw = originalKey.getEncoded();
            SecretKey secretKey = new SecretKeySpec(raw, SymmetricAlgorithm.AES.getValue());
            SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, secretKey);
            return aes.encryptHex(content);
        } catch (Exception e) {
            System.out.println("加密失败");
        }
        return null;
    }

    /**
     * 加密
     * 1.构造密钥生成器
     * 2.根据ecnodeRules规则初始化密钥生成器
     * 3.产生密钥
     * 4.创建和初始化密码器
     * 5.内容加密
     * 6.返回字符串
     */
    public static String AESDecode(String content, String encodeRules) {
        try {
            //1.构造密钥生成器，指定为AES算法,不区分大小写
            KeyGenerator keygen = KeyGenerator.getInstance(SymmetricAlgorithm.AES.getValue());
            //2.根据ecnodeRules规则初始化密钥生成器
            //生成一个128位的随机源,根据传入的字节数组
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(encodeRules.getBytes());
            keygen.init(128, random);
            //3.产生原始对称密钥
            SecretKey originalKey = keygen.generateKey();
            //4.获得原始对称密钥的字节数组
            byte[] raw = originalKey.getEncoded();
            SecretKey secretKey = new SecretKeySpec(raw, SymmetricAlgorithm.AES.getValue());
            SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, secretKey);
            return aes.decryptStr(content, CharsetUtil.CHARSET_UTF_8);
        } catch (Exception e) {
            System.out.println("解密失败");
        }
        return null;
    }

    public static void main(String[] args) {
        String[] rules = new String[]{"123-abc", "bbc-2345"};
        String content = "{'nickname':'honsun','email':'honsun@yaheen.io'}";
        for (String rule : rules) {
            String decode = AESEncode(content + rule, rule);
            System.out.println("decode:" + decode);
            String encode = AESDecode(decode, rule);
            System.out.println("encode:" + encode);
        }
    }

}
