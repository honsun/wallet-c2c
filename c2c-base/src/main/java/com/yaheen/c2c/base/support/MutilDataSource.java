package com.yaheen.c2c.base.support;

/**
 * FileName: MutilDataSource
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: 多数据源的从库中选择一个返回使用
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0
 */
public class MutilDataSource {

    public static final String[] slaves=new String[]{"slave1"};
    public static int currentIndex=0;

    /**
     * 轮询返回一个从库
     * @return
     */
    public static String getSalve(){
        if(currentIndex+1>slaves.length){
            currentIndex=0;
        }
        return slaves[currentIndex++];
    }
}
