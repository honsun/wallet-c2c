package com.yaheen.c2c.base;

import java.io.Serializable;

/**
 * FileName: YaheenResult
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: 逻辑返回结果基类
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0
 */
public class YaheenResult implements Serializable {

    //状态码(200成功，其他为失败)
    public int code;

    //成功为success，其他为失败原因
    public String message;

    //数据结果集
    public Object data;

    public YaheenResult(int code, String message) {
        this.code=code;
        this.message=message;
    }

    public YaheenResult(int code, String message, Object data) {
        this.code=code;
        this.message=message;
        this.data=data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
