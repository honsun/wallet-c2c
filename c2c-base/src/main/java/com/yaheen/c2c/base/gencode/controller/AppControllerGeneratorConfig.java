/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppModelGeneratorConfig
 * Author:   15820
 * Date:     2019/3/16 15:00
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.controller;

import io.jboot.app.config.annotation.ConfigModel;

/**
 * FileName: AppControllerGeneratorConfig
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: Controller生成器配置项
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@ConfigModel(prefix = "yaheen.c2c.controller.ge")
public class AppControllerGeneratorConfig {

    /** controller 包名*/
    private String controllerpackage;
    /** 移除的表前缀 */
    private String removedtablenameprefixes;
    /** 不包含表 */
    private String excludedtable;
    /** 不包含表前缀 */
    private String excludedtableprefixes;
    /** controller 模板文件 */
    private String controllertemplate;

    public String getRemovedtablenameprefixes() {
        return removedtablenameprefixes;
    }

    public void setRemovedtablenameprefixes(String removedtablenameprefixes) {
        this.removedtablenameprefixes = removedtablenameprefixes;
    }

    public String getExcludedtable() {
        return excludedtable;
    }

    public void setExcludedtable(String excludedtable) {
        this.excludedtable = excludedtable;
    }

    public String getExcludedtableprefixes() {
        return excludedtableprefixes;
    }

    public void setExcludedtableprefixes(String excludedtableprefixes) {
        this.excludedtableprefixes = excludedtableprefixes;
    }

    public String getControllerpackage() {
        return controllerpackage;
    }

    public void setControllerpackage(String controllerpackage) {
        this.controllerpackage = controllerpackage;
    }

    public String getControllertemplate() {
        return controllertemplate;
    }

    public void setControllertemplate(String controllertemplate) {
        this.controllertemplate = controllertemplate;
    }

    @Override
    public String toString() {
        return "AppGeneratorConfig{" +
                "controllerpackage='" + controllerpackage + '\'' +
                ", removedtablenameprefixes='" + removedtablenameprefixes + '\'' +
                ", excludedtable='" + excludedtable + '\'' +
                ", excludedtableprefixes='" + excludedtableprefixes + '\'' +
                ", controllertemplatecontrollertemplate='" + controllertemplate + '\'' +
                '}';
    }

}