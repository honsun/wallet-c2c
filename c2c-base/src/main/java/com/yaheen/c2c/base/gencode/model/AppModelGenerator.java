/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppModelGenerator
 * Author:   15820
 * Date:     2019/3/16 14:59
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.model;

import com.jfinal.kit.FileKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import io.jboot.Jboot;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.model.JbootBaseModelGenerator;
import io.jboot.codegen.model.JbootModelGenerator;
import io.jboot.utils.ArrayUtil;
import io.jboot.utils.FileUtil;

import javax.sql.DataSource;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author 15820
 * @create 2019/3/16
 * @since 1.0.0
 */
public class AppModelGenerator {

    public static void doGenerate() {
        AppModelGeneratorConfig config = Jboot.config(AppModelGeneratorConfig.class);

        System.out.println(config.toString());

        if (StrKit.isBlank(config.getModelpackage())) {
            System.err.println("yaheen.c2c.model.ge.modelpackage 不可为空");
            System.exit(0);
        }

        String modelPackage = config.getModelpackage();
        String baseModelPackage = modelPackage + ".base";

        String modelDir = PathKit.getWebRootPath() + "/src/main/java/" + modelPackage.replace(".", "/");
        String baseModelDir = PathKit.getWebRootPath() + "/src/main/java/" + baseModelPackage.replace(".", "/");

        //先清空目录再重新生成，避免删除数据库而无法删除项目对应的model类
        FileKit.delete(new File(baseModelDir));
        FileKit.delete(new File(modelDir));

        System.out.println("start generate...");
        System.out.println("generate dir:" + modelDir);

        DataSource dataSource = CodeGenHelpler.getDatasource();

        AppMetaBuilder metaBuilder = new AppMetaBuilder(dataSource);

        if (StrKit.notBlank(config.getRemovedtablenameprefixes())) {
            metaBuilder.setRemovedTableNamePrefixes(config.getRemovedtablenameprefixes().split(","));
        }

        if (StrKit.notBlank(config.getExcludedtableprefixes())) {
            metaBuilder.setSkipPre(config.getExcludedtableprefixes().split(","));
        }

        List<TableMeta> tableMetaList = metaBuilder.build();
        CodeGenHelpler.excludeTables(tableMetaList, config.getExcludedtable());

        new JbootBaseModelGenerator(baseModelPackage, baseModelDir).generate(tableMetaList);
        new JbootModelGenerator(modelPackage, baseModelPackage, modelDir).generate(tableMetaList);

        System.out.println("entity generate finished !!!");

    }


}