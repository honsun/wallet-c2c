package com.yaheen.c2c.base.util;

import cn.hutool.json.JSONUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import io.jboot.utils.ArrayUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * FileName: ModelUtil
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: Model工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0
 */
public class ModelUtil {

    /**
     * Record 转换 Model
     * @param modelClass
     * @param record
     * @param <T>
     * @return
     */
    public static <T extends Model> T toModel(Class<T> modelClass, Record record) {
        T model = null;
        try {
            model = modelClass.newInstance();
        } catch (Exception e) {
            return model;
        }
        if(null!=record){
            model.put(record);
        }
        return model;
    }

    /**
     * Model 转换 Redis存储的Map对象
     * @param modelClass
     * @param record
     * @param <T>
     * @return
     */
    public static Map<Object,Object> toRedisMap(Model<?> model){
        Map<String,Object> columnsMap=model.toRecord().getColumns();
        Map<Object,Object> redisMap=new HashMap<>();
        if(ArrayUtil.isNotEmpty(columnsMap)){
            Set<String> keys=columnsMap.keySet();
            for(String key:keys){
                if(StrKit.notNull(columnsMap.get(key)))
                    redisMap.put(key,columnsMap.get(key));
            }
        }
        return redisMap;
    }
}
