package com.yaheen.c2c.base.util;


import cn.hutool.core.util.RandomUtil;
import com.jfinal.kit.StrKit;
import com.yaheen.c2c.base.util.key.SnowflakeIdWorker;
import org.apache.commons.lang3.StringUtils;

/**
 * FileName: IdUtil
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: 分布式ID获取器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0             采用的是推特的ID生成算法改进版
 */
public class IdUtil {

    private static String[] alphabets = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    /**
     * 获取全局唯一ID
     *
     * @return
     */
    public static String nextId() {
        return String.valueOf(SnowflakeIdWorker.getInstance().nextId());
    }

    /**
     * 根据用户ID生成用户密钥
     * @param userId 用户ID
     * @return
     */
    public static String generateAppSecret(String userId) {
        StringBuffer sb=new StringBuffer();
        //获取用户ID最后一位数字
        int lastNum = Integer.parseInt(userId.substring(userId.length() - 1));
        for (int i = 0; i < userId.length(); i++) {
            //每一位数字必须加上最后一位数字得到总和
            int num=Integer.parseInt(userId.substring(i,i+1))+lastNum;
            //替换成对应的字母
            sb.append(alphabets[num]);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(nextId());
    }
}
