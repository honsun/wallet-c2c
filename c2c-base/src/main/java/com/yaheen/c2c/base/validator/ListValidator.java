package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;
import com.jfinal.json.FastJson;

import java.util.List;
import java.util.Map;

/**
 * FileName: ListValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 集合校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class ListValidator extends ValidatorHandler<Object> implements Validator<Object> {

    /**字段名称**/
    private String fieldName;

    public ListValidator(String fieldName){
        this.fieldName=fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Object s){
        if(null == s){
            context.addError(ValidationError.create(String.format("%s必须是数组集合",fieldName)));
            return false;
        }
        try{
            List<Map<String, Object>> list = FastJson.getJson().parse(s.toString(), List.class);
        }catch (Exception e){
            return false;
        }
        return true;
    }


}
