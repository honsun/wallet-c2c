package com.yaheen.c2c.base.common;

/**
 * FileName: HttpMethods
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: HTTP请求参数类型
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public interface DataType {

    String INTEGER="integer";
    String STRING="string";
    String LIST="list";
    String FILE="file";

}
