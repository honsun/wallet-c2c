package com.yaheen.c2c.base;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.jfinal.json.FastJson;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.upload.UploadFile;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.RedisKey;
import com.yaheen.c2c.base.exception.EmptyParamException;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.exception.ValidationException;
import com.yaheen.c2c.base.support.Decrypt;
import com.yaheen.c2c.base.util.IdUtil;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.db.model.JbootModel;
import io.jboot.support.redis.JbootRedis;
import io.jboot.utils.StrUtil;
import io.jboot.web.controller.JbootController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Map;

/**
 * FileName: BaseController
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: 基础控制器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0
 */
public class BaseController extends JbootController {

    public static JbootRedis redis = Jboot.getRedis();

    /**
     * 获取当前登录的userId
     *
     * @return
     */
    protected String currentPlatformId() {
        String platformId=getHeader(Constant.PLATFORM_ID);
        if(StrUtil.isNotBlank(platformId)){
            platformId=platformId.replaceAll("#","");
        }
        return platformId;
    }

    /**
     * 获取当前登录的userId
     *
     * @return
     */
    protected String currentUserId() {
        String userId=getHeader(Constant.USER_ID);
        if(StrUtil.isNotBlank(userId)){
            userId=userId.replaceAll("#","");
        }
        return userId;
    }

    /**
     * 将前端传递过来的Json转换成Map对象
     *
     * @return
     */
    protected Map<String, Object> jsonToMap(int comeFrom) {
        //获取前端传递的加密数据
        String requestData = HttpKit.readData(getRequest());
        System.out.println("requestData>>>" + requestData);
        if (StrKit.isBlank(requestData)) {
            return Collections.EMPTY_MAP;
        }
        requestData=requestData.replaceAll("#","");
        return FastJson.getJson().parse(requestData, Map.class);
        /*
        String needCheck = Jboot.getRedis().hget(RedisKey.CACHE_RESOURCE, getRequest().getRequestURI());
        if (1 == comeFrom) {  //前端调用
            //判断是否需要解密数据
            if ("1".equals(needCheck)) {
                //得到解密后的真实数据
                String realData = Decrypt.AESDecode(requestData, currentPlatformId() + "-" + currentUserId());
                if (StrKit.isBlank(realData)) {
                    throw new EmptyParamException();
                }
                return FastJson.getJson().parse(realData, Map.class);
            } else {
                return FastJson.getJson().parse(requestData, Map.class);
            }
        } else {  //第三方接口调用
            if ("1".equals(needCheck)) {
                //得到解密后的真实数据
                String realData = Decrypt.AESDecode(requestData, currentPlatformId());
                if (StrKit.isBlank(realData)) {
                    throw new EmptyParamException();
                }
                return FastJson.getJson().parse(realData, Map.class);
            } else {
                return FastJson.getJson().parse(requestData, Map.class);
            }
        }
        */
    }

    /**
     * 将前端传递过来的Json转换成对应的Model对象
     *
     * @param t        Model对象类
     * @param <T>      返回对应的Model对象
     * @param comeFrom 从哪里来 1:前端调用 2:外部接口
     * @return
     */
    public <T extends Model> T jsonToModel(Class<T> t, boolean allowEmpty, int comeFrom) {
        Map<String, Object> map = jsonToMap(comeFrom);
        if (!allowEmpty && MapUtil.isEmpty(map)) {
            throw new EmptyParamException();
        }
        try {
            T model = t.newInstance();
            if (MapUtil.isNotEmpty(map)) {
                model.put(map);
            }
            model.put(JbootModel.AUTO_COPY_MODEL, true);
            return model;
        } catch (Exception e) {
            throw new HandlerException("The parameter format must conform to the JSON specification");
        }
    }

    /**
     * 单文件上传
     *
     * @param file 文件
     * @return
     */
    protected String uploadFile(UploadFile file) {
        if (null == file) {
            return "";
        }
        //设置唯一文件名
        String filename = new StringBuilder(RandomUtil.randomString(8)).append(".").append(FileUtil.extName(file.getFileName())).toString();
        //创建文件目录
        File imageFile = new File(Constant.uploadPath);
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        File localFile = new File(Constant.uploadPath + filename);
        file.getFile().renameTo(localFile);
        file.getFile().delete();
        return filename;
    }

    /**
     * 单文件上传
     *
     * @param file 文件
     * @return
     */
    protected String uploadBase64(String base64Code, String filetype) {
        if (!StrKit.notBlank(base64Code, filetype)) {
            throw new HandlerException("Upload error");
        }
        //设置唯一文件名
        String filename = new StringBuilder(RandomUtil.randomString(8)).append(".").append(filetype).toString();
        //创建文件目录
        File imageFile = new File(Constant.uploadPath);
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        try {
            byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
            FileOutputStream out = new FileOutputStream(Constant.uploadPath + filename);
            out.write(buffer);
            out.close();
        } catch (Exception e) {
            throw new HandlerException("Upload error");
        }
        return filename;
    }

    /**
     * 统一处理校验结果
     *
     * @param complex
     */
    protected void validation(ComplexResult complex) {
        if (!complex.isSuccess()) {
            throw new ValidationException(complex.getErrors());
        }
    }

    /**
     * 成功返回
     *
     * @param data
     */
    protected void SuccessResult(Object data) {
        renderJson(new YaheenResult(Constant.SUCCESS_CODE, Constant.SUCCESS, data));
    }

    /**
     * 成功返回
     */
    protected void SuccessResult() {
        renderJson(new YaheenResult(Constant.SUCCESS_CODE, Constant.SUCCESS, "The request was processed successfully"));
    }

    /**
     * 失败返回
     *
     * @param data
     */
    protected void FailedResult(Object data) {
        renderJson(new YaheenResult(Constant.FAILED_CODE, Constant.FAILED, data));
    }

}
