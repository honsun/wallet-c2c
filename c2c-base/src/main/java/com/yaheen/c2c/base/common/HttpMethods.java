package com.yaheen.c2c.base.common;

/**
 * FileName: HttpMethods
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: HTTP请求方法类型
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public interface HttpMethods {

    String OPTIONS = "options";
    String GET = "get";
    String POST = "post";
    String PUT = "put";
    String DELETE = "delete";

}
