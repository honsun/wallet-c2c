package com.yaheen.c2c.base.gencode;

import com.yaheen.c2c.base.gencode.model.DataSourceConfig;
import io.jboot.Jboot;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

/**
 * FileName: ScanTable
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 扫描数据库获取表及字段的定义和注释
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class ScanTable {
    private final static Logger logger = LoggerFactory.getLogger(ScanTable.class);

    private static final String SQL = "SELECT * FROM ";// 数据库操作
    private static boolean print = false;

    public static void main(String[] args) throws Exception {
        print = true;
        getTableMap();
    }

    public static Map<String, Map<String, String>> getTableMap() throws Exception {
        Map<String, Map<String, String>> tableMap = new HashMap<>();
        //第一步：获取数据库所有表名
        List<String> tableNames = getTableNames();
        if (ArrayUtil.isNotEmpty(tableNames)) {
            //第二步：收集获取表下字段的所有元信息
            for (String tableName : tableNames) {
                tableMap.put(tableName, getColumnComments(tableName));
            }
            if (print) {
                Set<String> keys = tableMap.keySet();
                for (String key : keys) {
                    System.out.println("表名：" + key);
                    Map<String, String> commentMap = tableMap.get(key);
                    if (ArrayUtil.isNotEmpty(commentMap)) {
                        Set<String> commentKeys = commentMap.keySet();
                        for (String commentKey : commentKeys) {
                            System.out.println("字段名：" + commentKey + "，注释：" + commentMap.get(commentKey));
                        }
                    }
                    System.out.println("------------------------------------------");
                }
            }
        }
        return tableMap;
    }

    /**
     * 获取表中字段的所有注释
     *
     * @param tableName
     * @return
     */
    public static Map<String, String> getColumnComments(String tableName) {
        Map<String, String> commentMap = new HashMap<>();
        //与数据库的连接
        Connection conn = getConnection();
        PreparedStatement pStemt = null;
        String tableSql = SQL + tableName;
        ResultSet rs = null;
        try {
            pStemt = conn.prepareStatement(tableSql);
            rs = pStemt.executeQuery("show full columns from " + tableName);
            while (rs.next()) {
                commentMap.put(rs.getString("Field"), rs.getString("Comment"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                    closeConnection(conn);
                } catch (SQLException e) {
                    logger.error("getColumnComments close ResultSet and connection failure", e);
                }
            }
        }
        return commentMap;
    }

    /**
     * 获取数据库下的所有表名
     */
    public static List<String> getTableNames() {
        List<String> tableNames = new ArrayList<>();
        Connection conn = getConnection();
        ResultSet rs = null;
        try {
            //获取数据库的元数据
            DatabaseMetaData db = conn.getMetaData();
            //从元数据中获取到所有的表名
            rs = db.getTables(null, null, null, new String[]{"TABLE"});
            while (rs.next()) {
                tableNames.add(rs.getString(3));
            }
        } catch (SQLException e) {
            logger.error("getTableNames failure", e);
        } finally {
            try {
                rs.close();
                closeConnection(conn);
            } catch (SQLException e) {
                logger.error("close ResultSet failure", e);
            }
        }
        return tableNames;
    }

    /**
     * 获取数据库连接
     *
     * @return
     */
    public static Connection getConnection() {
        Connection conn = null;
        try {
            DataSourceConfig config= Jboot.config(DataSourceConfig.class);
            System.out.println(config.toString());
            conn = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());
        } catch (SQLException e) {
            logger.error("get connection failure", e.getMessage());
        }
        return conn;
    }

    /**
     * 关闭数据库连接
     *
     * @param conn
     */
    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                logger.error("close connection failure", e);
            }
        }
    }
}
