/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppServiceImplGeneratorConfig
 * Author:   15820
 * Date:     2019/3/16 15:53
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.serviceimpl;

import io.jboot.app.config.annotation.ConfigModel;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author honsun
 * @create 2019/3/16
 * @since 1.0.0
 */
@ConfigModel(prefix="yaheen.c2c.serviceimpl.ge")
public class AppServiceImplGeneratorConfig {

    /** entity 包名 */
    private String modelpackage;
    /** service 包名 */
    private String servicepackage;
    /** serviceimpl 包名 */
    private String serviceimplpackage;
    /** 移除的表前缀 */
    private String removedtablenameprefixes;
    /** 不包含表 */
    private String excludedtable;
    /** 不包含表前缀 */
    private String excludedtableprefixes;

    public String getModelpackage() {
        return modelpackage;
    }

    public void setModelpackage(String modelpackage) {
        this.modelpackage = modelpackage;
    }

    public String getServicepackage() {
        return servicepackage;
    }

    public void setServicepackage(String servicepackage) {
        this.servicepackage = servicepackage;
    }

    public String getRemovedtablenameprefixes() {
        return removedtablenameprefixes;
    }

    public void setRemovedtablenameprefixes(String removedtablenameprefixes) {
        this.removedtablenameprefixes = removedtablenameprefixes;
    }

    public String getExcludedtable() {
        return excludedtable;
    }

    public void setExcludedtable(String excludedtable) {
        this.excludedtable = excludedtable;
    }

    public String getExcludedtableprefixes() {
        return excludedtableprefixes;
    }

    public void setExcludedtableprefixes(String excludedtableprefixes) {
        this.excludedtableprefixes = excludedtableprefixes;
    }

    public String getServiceimplpackage() {
        return serviceimplpackage;
    }

    public void setServiceimplpackage(String serviceimplpackage) {
        this.serviceimplpackage = serviceimplpackage;
    }

    @Override
    public String toString() {
        return "AppServiceImplGeneratorConfig{" +
                "modelpackage='" + modelpackage + '\'' +
                ", servicepackage='" + servicepackage + '\'' +
                ", serviceimplpackage='" + serviceimplpackage + '\'' +
                ", removedtablenameprefixes='" + removedtablenameprefixes + '\'' +
                ", excludedtable='" + excludedtable + '\'' +
                ", excludedtableprefixes='" + excludedtableprefixes + '\'' +
                '}';
    }
}