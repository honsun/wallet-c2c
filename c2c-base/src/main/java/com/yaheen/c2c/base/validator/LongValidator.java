package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;

/**
 * FileName: LongValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 整形校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class LongValidator extends ValidatorHandler<Object> implements Validator<Object>{

    /**字段名称**/
    private String fieldName;
    /**最小长度**/
    private int min=-1;
    /**最大长度**/
    private int max=-1;

    public LongValidator(String fieldName, int min, int max){
        this.min=min;
        this.max=max;
        this.fieldName=fieldName;
    }

    public LongValidator(String fieldName){
        this.fieldName=fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context,Object s){
        if(s==null){
            context.addError(ValidationError.create(String.format("%s不能为空",fieldName)));
            return false;
        }
        if(!(s instanceof Integer) && !(s instanceof Long)){
            context.addError(ValidationError.create(String.format("%s必须是整数型",fieldName)));
            return false;
        }
        try{
            Long value=Long.parseLong(s.toString());
            if(max>=0){
                if(value>max || value<min){
                    context.addError(ValidationError.create(String.format("%s长度必须介于%s~%s之间",fieldName,min,max)));
                    return false;
                }
            }
        }catch (NumberFormatException ex){
            context.addError(ValidationError.create(String.format("%s只能是整数型",fieldName)));
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        String status="-1";
        System.out.println(Long.parseLong(status));
    }
}
