package com.yaheen.c2c.base.exception;

/**
 * FileName: HandlerException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 逻辑处理异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class HandlerException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String errorMessage;

    public HandlerException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public HandlerException() {
        super("An error occurred during the request. Please try again");
        this.errorMessage = "An error occurred during the request. Please try again";
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
