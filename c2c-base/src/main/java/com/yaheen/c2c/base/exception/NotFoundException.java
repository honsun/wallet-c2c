package com.yaheen.c2c.base.exception;

/**
 * FileName: NotFoundException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 空资源异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private static final String errorMessage="The resource was not found";

    public NotFoundException() {
       super(errorMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
