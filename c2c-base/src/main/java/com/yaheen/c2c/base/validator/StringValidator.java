package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;

/**
 * FileName: StringValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 字符串校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class StringValidator extends ValidatorHandler<Object> implements Validator<Object>{

    /**字段名称**/
    private String fieldName;
    /**最小长度**/
    private int min;
    /**最大长度**/
    private int max;

    public StringValidator(String fieldName, int min, int max){
        this.min=min;
        this.max=max;
        this.fieldName=fieldName;
    }

    public StringValidator(String fieldName){
        this.fieldName=fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context,Object s){
        if(s==null){
            context.addError(ValidationError.create(String.format("%s不能为空",fieldName)));
            return false;
        }
        if(!(s instanceof String)){
            context.addError(ValidationError.create(String.format("%s必须是字符串",fieldName)));
            return false;
        }
        String value=s.toString();
        if(max>0){
            if(value.length()>max || value.length()<min){
                if(min==max){
                    context.addError(ValidationError.create(String.format("%s长度为%s位",fieldName,min)));
                    return false;
                }else{
                    context.addError(ValidationError.create(String.format("%s长度必须介于%s~%s之间",fieldName,min,max)));
                    return false;
                }
            }
        }
        return true;
    }
}
