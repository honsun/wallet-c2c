package com.yaheen.c2c.base.interceptor;

import cn.hutool.core.date.SystemClock;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.utils.JsonUtils;
import com.yaheen.c2c.base.YaheenResult;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * FileName: ExceptionInterceptor
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 异常处理中心
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class ExceptionInterceptor implements Interceptor {

    private Logger logger = LoggerFactory.getLogger(ExceptionInterceptor.class);

    @Override
    public void intercept(Invocation ai) {
        try {
            //通过方法的执行前后时间计算方法总体执行时间
            long start = SystemClock.now();
            ai.invoke();
            long consume = SystemClock.now() - start;
            //超过阀值记录下来，后续优先考虑优化
            if (consume > 5) {
                logger.info("Interface[{}] consume:{} ms", ai.getController().getRequest().getRequestURI(), consume);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            StringBuilder sb = new StringBuilder();
            YaheenResult result = null;
            if (e instanceof HandlerException) {  //处理错误
                HandlerException errorParam = (HandlerException) e;
                result = new YaheenResult(Constant.FAILED_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else if (e instanceof NotFoundException) {  //资源不存在
                NotFoundException errorParam = (NotFoundException) e;
                result = new YaheenResult(Constant.NOT_FOUND_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else if (e instanceof NoLoginException) {  //未登录
                NoLoginException errorParam = (NoLoginException) e;
                result = new YaheenResult(Constant.NO_PERMISSION_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else if (e instanceof NoPermissionException) {  //无权限访问资源
                NoPermissionException errorParam = (NoPermissionException) e;
                result = new YaheenResult(Constant.NO_PERMISSION_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else if (e instanceof EmptyParamException) { //空参数错误
                EmptyParamException errorParam = (EmptyParamException) e;
                result = new YaheenResult(Constant.FAILED_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else if (e instanceof ValidationException) {  //参数验证错误
                ValidationException errorParam = (ValidationException) e;
                result = new YaheenResult(Constant.FAILED_CODE, Constant.FAILED,
                        sb.append(StrKit.notBlank(errorParam.getErrorMessage()) ? errorParam.getErrorMessage() : e.getMessage()).toString());
            } else {
                logger.info(e.getMessage());
                result = new YaheenResult(Constant.FAILED_CODE, Constant.FAILED, "An error occurred during the request. Please try again");
                e.printStackTrace();
            }
            HttpServletRequest request = ai.getController().getRequest();
            logger.error("Request uri:{}", request.getRequestURI());
            logger.error("Request parameters：{}", request.getAttribute("request_data"));
            logger.error("Error info：{}", JsonUtils.toJson(result));
            System.out.println(JsonUtils.toJson(result));
            ai.getController().renderJson(result);
        }
    }

}