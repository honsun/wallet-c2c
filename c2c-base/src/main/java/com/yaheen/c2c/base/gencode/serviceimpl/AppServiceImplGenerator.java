/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppServiceImplGenerator
 * Author:   15820
 * Date:     2019/3/16 15:52
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.serviceimpl;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.yaheen.c2c.base.gencode.model.AppMetaBuilder;
import io.jboot.Jboot;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.service.JbootServiceImplGenerator;

import javax.sql.DataSource;
import java.util.List;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author 15820
 * @create 2019/3/16
 * @since 1.0.0
 */
public class AppServiceImplGenerator {


    public static void doGenerate() {
        AppServiceImplGeneratorConfig config = Jboot.config(AppServiceImplGeneratorConfig.class);

        System.out.println(config.toString());

        if (StrKit.isBlank(config.getModelpackage())) {
            System.err.println("yaheen.c2c.serviceimpl.ge.modelpackage 不可为空");
            System.exit(0);
        }

        if (StrKit.isBlank(config.getServicepackage())) {
            System.err.println("yaheen.c2c.serviceimpl.ge.servicepackage 不可为空");
            System.exit(0);
        }

        if (StrKit.isBlank(config.getServiceimplpackage())) {
            System.err.println("yaheen.c2c.serviceimpl.ge.serviceimplpackage 不可为空");
            System.exit(0);
        }

        String modelPackage = config.getModelpackage();
        String servicepackage = config.getServicepackage();
        String serviceimplpackage = config.getServiceimplpackage();

        System.out.println("start generate...");
        System.out.println("generate dir:" + servicepackage);

        DataSource dataSource = CodeGenHelpler.getDatasource();

        AppMetaBuilder metaBuilder = new AppMetaBuilder(dataSource);

        if (StrKit.notBlank(config.getRemovedtablenameprefixes())) {
            metaBuilder.setRemovedTableNamePrefixes(config.getRemovedtablenameprefixes().split(","));
        }

        if (StrKit.notBlank(config.getExcludedtableprefixes())) {
            metaBuilder.setSkipPre(config.getExcludedtableprefixes().split(","));
        }

        List<TableMeta> tableMetaList = metaBuilder.build();
        CodeGenHelpler.excludeTables(tableMetaList, config.getExcludedtable());

        //new AppJbootServiceImplGenerator(servicepackage , modelPackage, serviceimplpackage).generate(tableMetaList);
        JbootServiceImplGenerator jbootServiceImplGenerator=new JbootServiceImplGenerator(servicepackage,modelPackage);
        jbootServiceImplGenerator.setImplName("provider");
        jbootServiceImplGenerator.generate(tableMetaList);
        System.out.println("service generate finished !!!");

    }
}