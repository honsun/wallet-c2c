/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppServiceGenerator
 * Author:   15820
 * Date:     2019/3/16 15:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.service;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.yaheen.c2c.base.gencode.model.AppMetaBuilder;
import io.jboot.Jboot;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.codegen.service.JbootServiceInterfaceGenerator;

import javax.sql.DataSource;
import java.util.List;

/**
 * FileName: AppServiceGenerator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: Controller生成器配置项
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class AppServiceGenerator {

    public static void doGenerate() {
        AppServiceGeneratorConfig config = Jboot.config(AppServiceGeneratorConfig.class);

        System.out.println(config.toString());

        if (StrKit.isBlank(config.getModelpackage())) {
            System.err.println("yaheen.c2c.service.ge.modelpackage 不可为空");
            System.exit(0);
        }

        if (StrKit.isBlank(config.getServicepackage())) {
            System.err.println("yaheen.c2cservice.ge.servicepackage 不可为空");
            System.exit(0);
        }

        String modelPackage = config.getModelpackage();
        String servicepackage = config.getServicepackage();

        System.out.println("start generate...");
        System.out.println("generate dir:" + servicepackage);

        DataSource dataSource = CodeGenHelpler.getDatasource();

        AppMetaBuilder metaBuilder = new AppMetaBuilder(dataSource);

        if (StrKit.notBlank(config.getRemovedtablenameprefixes())) {
            metaBuilder.setRemovedTableNamePrefixes(config.getRemovedtablenameprefixes().split(","));
        }

        if (StrKit.notBlank(config.getExcludedtableprefixes())) {
            metaBuilder.setSkipPre(config.getExcludedtableprefixes().split(","));
        }

        List<TableMeta> tableMetaList = metaBuilder.build();
        CodeGenHelpler.excludeTables(tableMetaList, config.getExcludedtable());

        new JbootServiceInterfaceGenerator(servicepackage, modelPackage).generate(tableMetaList);

        System.out.println("service generate finished !!!");

    }

}