package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;

/**
 * FileName: FileValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 文件类型校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class FileValidator extends ValidatorHandler<String> implements Validator<String> {

    /**字段名称**/
    private String fieldName;

    /**文件后缀名，可多个**/
    private String extensions;

    public FileValidator(String fieldName,String extensions){
        this.fieldName=fieldName;
        this.extensions=extensions;
    }

    @Override
    public boolean validate(ValidatorContext context, String s){
        if(null == s){
            context.addError(ValidationError.create(String.format("%s不能为空",fieldName)));
            return false;
        }
        String[] exts=extensions.split(",");
        boolean match=false;
        for(String ext:exts){
            if(s.equalsIgnoreCase(ext) || s.endsWith("."+ext)){
                match=true;
            }
        }
        if(!match){
            context.addError(ValidationError.create(String.format("%s格式错误",fieldName)));
            return false;
        }
        return true;
    }


}
