package com.yaheen.c2c.base.util;

import com.jfinal.json.FastJson;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import io.jboot.db.model.JbootModel;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * FileName: StringUtil
 * Author:   Honsun
 * Date:     2019/3/21 17:43
 * Description: String工具类
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/21 17:43     2.0.0
 */
public class StringUtil {

    private static Logger logger = LoggerFactory.getLogger(StringUtil.class);

    /**
     * String转换成List
     *
     * @param str
     * @return
     */
    public static List<String> stringToStringList(String str) {
        if (StrKit.notBlank(str)) {
            Set<String> set = null;
            try {
                set = FastJson.getJson().parse(str, Set.class);
            } catch (Exception e) {
                return Collections.EMPTY_LIST;
            }
            if (set != null && set.size() > 0) {
                List<String> list = new ArrayList<>();
                for (Object item : set) {
                    if (StrKit.notBlank(item.toString())) {
                        list.add(item.toString());
                    }
                }
                if (ArrayUtil.isNotEmpty(list)) {
                    return list;
                }
            }
        }
        return Collections.EMPTY_LIST;
    }

    /**
     * 将前端传递过来的Json转换成对应的Model对象集合
     *
     * @param t   Model对象类
     * @param <T> 返回对应的Model对象
     * @return
     */
    public static <T extends Model> List<T> jsonToList(String jsonString, Class<T> t) {
        if (StrKit.notBlank(jsonString)) {
            List<Map<String, Object>> list = null;
            try {
                list = FastJson.getJson().parse(jsonString, List.class);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            if (ArrayUtil.isNotEmpty(list)) {
                List<T> models = new ArrayList<>();
                try {
                    for (Map<String, Object> map : list) {
                        T model = t.newInstance();
                        model.put(map);
                        model.put(JbootModel.AUTO_COPY_MODEL, true);
                        models.add(model);
                    }
                } catch (InstantiationException e) {
                    logger.error(e.getMessage());
                } catch (IllegalAccessException e) {
                    logger.error(e.getMessage());
                } finally {
                    return models;
                }
            }
        }
        return Collections.EMPTY_LIST;
    }


    /**
     * 将前端传递过来的Json转换成对应的Model对象集合
     *
     * @param t   Model对象类
     * @param <T> 返回对应的Model对象
     * @return
     */
    public static <T extends Model> T jsonToObject(String jsonString, Class<T> t) {
        if (StrKit.notBlank(jsonString)) {
            Map<String, Object> map = null;
            try {
                map = FastJson.getJson().parse(jsonString, Map.class);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
            if (ArrayUtil.isNotEmpty(map)) {
                T model = null;
                try {
                    model = t.newInstance();
                    model.put(map);
                    model.put(JbootModel.AUTO_COPY_MODEL, true);
                } catch (InstantiationException e) {
                    logger.error(e.getMessage());
                } catch (IllegalAccessException e) {
                    logger.error(e.getMessage());
                } finally {
                    return model;
                }
            }
        }
        return null;
    }

    /**
     * 转义正则特殊字符 （$()*+.[]?\^{},|）
     *
     * @param keyword
     * @return
     */
    public static String escapeExprSpecialWord(String keyword) {
        if (StrKit.notBlank(keyword)) {
            String[] fbsArr = {"\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|", "'"};
            for (String key : fbsArr) {
                if (keyword.contains(key)) {
                    keyword = keyword.replace(key, "\\" + key);
                }
            }
        }
        return keyword;
    }

    /**
     * 判断是否图片格式文件
     * @param filename
     * @return
     */
    public static boolean isImage(String filename) {
        if (filename.endsWith(".jpg") || filename.endsWith(".JPG") || filename.endsWith(".gif") ||
                filename.endsWith(".GIF") || filename.endsWith(".png") || filename.endsWith(".PNG")){
            return true;
        }
        return false;
    }

}
