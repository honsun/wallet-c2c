package com.yaheen.c2c.base.common;

import com.jfinal.kit.PathKit;

import java.io.File;
import java.math.BigDecimal;

/**
 * FileName: Constant
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 系统全局变量
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class Constant {

    /**请求正确*/
    public static final int SUCCESS_CODE=200;
    /**请求错误*/
    public static final int FAILED_CODE=500;
    /**资源不存在*/
    public static final int NOT_FOUND_CODE=404;
    /**无权限访问*/
    public static final int NO_PERMISSION_CODE=666;

    public static final String SUCCESS="success";
    public static final String FAILED="failed";
    public static final String NOT_PERMISSION_MESSAGE="抱歉，您没有权限操作该资源";

    /**文件上传路径**/
    public static String uploadPath=new StringBuilder(PathKit.getWebRootPath()).append(File.separator).append("upload").append(File.separator).toString();

    /**分页参数**/
    public static final String pageNum = "pageNum";
    public static final String pageNumDes = "当前页码";
    public static final String pageSize = "pageSize";
    public static final String pageSizeDes = "每页数量";

    /**前端body唯一参数**/
    public static final String ENCRYPT_DATA = "encrypt_data";
    /**前端header唯一参数**/
    public static final String PLATFORM_ID = "platform_id";
    public static final String USER_ID = "user_id";

    public static final BigDecimal zero=new BigDecimal("0").setScale(4, BigDecimal.ROUND_DOWN);

}
