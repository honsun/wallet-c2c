package com.yaheen.c2c.base.exception;

import cn.hutool.core.collection.CollUtil;
import com.baidu.unbiz.fluentvalidator.ValidationError;

import java.util.List;

/**
 * FileName: ValidationException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 参数校验异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String errorMessage;

    public ValidationException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public ValidationException(List<ValidationError> errors) {
        StringBuilder sb = new StringBuilder();
        if (CollUtil.isNotEmpty(errors)) {
            for (ValidationError error : errors) {
                sb.append(error.getErrorMsg()).append("\n");
            }
        }
        throw new ValidationException(sb.toString());
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
