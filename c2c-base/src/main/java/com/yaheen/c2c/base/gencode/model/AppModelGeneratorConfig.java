/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppModelGeneratorConfig
 * Author:   15820
 * Date:     2019/3/16 15:00
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.model;

import io.jboot.app.config.annotation.ConfigModel;

/**
 * 〈一句话功能简述〉<br> 
 * 〈〉
 *
 * @author 15820
 * @create 2019/3/16
 * @since 1.0.0
 */
@ConfigModel(prefix="yaheen.c2c.model.ge")
public class AppModelGeneratorConfig {

    /** entity 包名 */
    private String modelpackage;
    /** 移除的表前缀 */
    private String removedtablenameprefixes;
    /** 不包含表 */
    private String excludedtable;
    /** 不包含表前缀 */
    private String excludedtableprefixes;

    public String getModelpackage() {
        return modelpackage;
    }

    public void setModelpackage(String modelpackage) {
        this.modelpackage = modelpackage;
    }

    public String getRemovedtablenameprefixes() {
        return removedtablenameprefixes;
    }

    public void setRemovedtablenameprefixes(String removedtablenameprefixes) {
        this.removedtablenameprefixes = removedtablenameprefixes;
    }

    public String getExcludedtable() {
        return excludedtable;
    }

    public void setExcludedtable(String excludedtable) {
        this.excludedtable = excludedtable;
    }

    public String getExcludedtableprefixes() {
        return excludedtableprefixes;
    }

    public void setExcludedtableprefixes(String excludedtableprefixes) {
        this.excludedtableprefixes = excludedtableprefixes;
    }

    @Override
    public String toString() {
        return "AppGeneratorConfig{" +
                "modelpackage='" + modelpackage + '\'' +
                ", removedtablenameprefixes='" + removedtablenameprefixes + '\'' +
                ", excludedtable='" + excludedtable + '\'' +
                ", excludedtableprefixes='" + excludedtableprefixes + '\'' +
                '}';
    }

}