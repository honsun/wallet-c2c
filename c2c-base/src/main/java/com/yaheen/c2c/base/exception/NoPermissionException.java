package com.yaheen.c2c.base.exception;

/**
 * FileName: NoPermissionException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 无权限访问异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class NoPermissionException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private static final String errorMessage="No permission to access this resource";

    public NoPermissionException() {
       super(errorMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
