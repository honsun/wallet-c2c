package com.yaheen.c2c.base.validator;

import com.baidu.unbiz.fluentvalidator.ValidationError;
import com.baidu.unbiz.fluentvalidator.Validator;
import com.baidu.unbiz.fluentvalidator.ValidatorContext;
import com.baidu.unbiz.fluentvalidator.ValidatorHandler;
import com.jfinal.kit.StrKit;
import io.jboot.utils.StrUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FileName: EmailValidator
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 邮箱地址校验器
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
public class EmailValidator extends ValidatorHandler<Object> implements Validator<Object> {

    /**
     * 字段名称
     **/
    private String fieldName;

    public EmailValidator(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(ValidatorContext context, Object s) {
        if (null == s) {
            context.addError(ValidationError.create(String.format("%s不能为空", fieldName)));
            return false;
        }
        String email = s.toString();
        if (!StrUtil.isEmail(email)) {
            context.addError(ValidationError.create(String.format("%s格式错误", fieldName)));
            return false;
        }
        return true;
    }
}