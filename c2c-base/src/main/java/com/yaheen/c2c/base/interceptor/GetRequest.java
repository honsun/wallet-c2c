package com.yaheen.c2c.base.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.yaheen.c2c.base.YaheenResult;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.HttpMethods;

/**
 * FileName: GetRequest
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 判断该请求是否Get类型
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class GetRequest implements Interceptor {

    @Override
    public void intercept(Invocation invocation) {
        Controller controller = invocation.getController();
        if (HttpMethods.OPTIONS.equalsIgnoreCase(controller.getRequest().getMethod())) {
            controller.renderJson(Constant.SUCCESS);
        }
        if (!HttpMethods.GET.equalsIgnoreCase(controller.getRequest().getMethod())) {
            controller.renderJson(new YaheenResult(Constant.FAILED_CODE, "该资源只能接受GET类型请求"));
        } else {
            invocation.invoke();
        }
    }
}
