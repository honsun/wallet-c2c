package com.yaheen.c2c.base.common;

/**
 * FileName: RedisKey
 * Author:   Honsun
 * Date:     2019/3/26 17:38
 * Description: Redis业务Key
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/26 17:38     2.0.0
 */
public class RedisKey {

	/**失效时间(单位：秒)**/
	public static final int CACHE_EXPIRE_TIME=60;
	/**获取用户信息**/
	public static final String CACHE_GET_USERINFO="cache_get_userinfo";
	/**预支付订单*/
	public static final String CACHE_PREPAY_ID="cache_prepay_id";
	/**系统资源*/
    public static final String CACHE_RESOURCE = "cache_resource";
}
