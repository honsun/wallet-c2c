package com.yaheen.c2c.base.exception;

/**
 * FileName: EmptyParamException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 空参数异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class EmptyParamException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private static String errorMessage="The request is missing the required parameters";

    public EmptyParamException() {
       super(errorMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
