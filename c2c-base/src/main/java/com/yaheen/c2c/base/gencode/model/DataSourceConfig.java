/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: DataSourceConfig
 * Author:   15820
 * Date:     2019/3/16 15:28
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.model;

import io.jboot.app.config.annotation.ConfigModel;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author 15820
 * @create 2019/3/16
 * @since 1.0.0
 */
@ConfigModel(prefix = "jboot.datasource")
public class DataSourceConfig {

    private String driver;
    private String url;
    private String user;
    private String password;

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DataSourceConfig{" +
                "driver='" + driver + '\'' +
                ", url='" + url + '\'' +
                ", user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}