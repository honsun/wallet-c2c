package com.yaheen.c2c.base.exception;

/**
 * FileName: NoLoginException
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 未登录异常
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class NoLoginException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private static final String errorMessage="Please login ";

    public NoLoginException() {
       super(errorMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
