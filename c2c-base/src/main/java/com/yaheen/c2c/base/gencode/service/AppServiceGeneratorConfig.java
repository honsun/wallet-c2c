/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: AppServiceGeneratorConfig
 * Author:   15820
 * Date:     2019/3/16 15:41
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.base.gencode.service;

import io.jboot.app.config.annotation.ConfigModel;

/**
 * FileName: AppServiceGeneratorConfig
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: Controller生成器配置项
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@ConfigModel(prefix="yaheen.c2c.service.ge")
public class AppServiceGeneratorConfig {

    /** entity 包名 */
    private String modelpackage;
    /** service 包名 */
    private String servicepackage;
    /** 移除的表前缀 */
    private String removedtablenameprefixes;
    /** 不包含表 */
    private String excludedtable;
    /** 不包含表前缀 */
    private String excludedtableprefixes;

    public String getModelpackage() {
        return modelpackage;
    }

    public void setModelpackage(String modelpackage) {
        this.modelpackage = modelpackage;
    }

    public String getServicepackage() {
        return servicepackage;
    }

    public void setServicepackage(String servicepackage) {
        this.servicepackage = servicepackage;
    }

    public String getRemovedtablenameprefixes() {
        return removedtablenameprefixes;
    }

    public void setRemovedtablenameprefixes(String removedtablenameprefixes) {
        this.removedtablenameprefixes = removedtablenameprefixes;
    }

    public String getExcludedtable() {
        return excludedtable;
    }

    public void setExcludedtable(String excludedtable) {
        this.excludedtable = excludedtable;
    }

    public String getExcludedtableprefixes() {
        return excludedtableprefixes;
    }

    public void setExcludedtableprefixes(String excludedtableprefixes) {
        this.excludedtableprefixes = excludedtableprefixes;
    }

    @Override
    public String toString() {
        return "AppServiceGeneratorConfig{" +
                "modelpackage='" + modelpackage + '\'' +
                ", servicepackage='" + servicepackage + '\'' +
                ", removedtablenameprefixes='" + removedtablenameprefixes + '\'' +
                ", excludedtable='" + excludedtable + '\'' +
                ", excludedtableprefixes='" + excludedtableprefixes + '\'' +
                '}';
    }
}