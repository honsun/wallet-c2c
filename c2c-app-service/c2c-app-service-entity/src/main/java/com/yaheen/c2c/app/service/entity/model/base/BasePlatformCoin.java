package com.yaheen.c2c.app.service.entity.model.base;

import io.jboot.db.model.JbootModel;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by Jboot, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePlatformCoin<M extends BasePlatformCoin<M>> extends JbootModel<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public void setPlatformId(java.lang.String platformId) {
		set("platform_id", platformId);
	}
	
	public java.lang.String getPlatformId() {
		return getStr("platform_id");
	}

	public void setCoinId(java.lang.String coinId) {
		set("coin_id", coinId);
	}
	
	public java.lang.String getCoinId() {
		return getStr("coin_id");
	}

	public void setCoinName(java.lang.String coinName) {
		set("coin_name", coinName);
	}
	
	public java.lang.String getCoinName() {
		return getStr("coin_name");
	}

	public void setBasis(java.lang.Integer basis) {
		set("basis", basis);
	}
	
	public java.lang.Integer getBasis() {
		return getInt("basis");
	}

	public void setDescription(java.lang.String description) {
		set("description", description);
	}
	
	public java.lang.String getDescription() {
		return getStr("description");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
