
package com.yaheen.c2c.app.service.entity.gencode;

import cn.hutool.core.io.file.FileWriter;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.generator.ColumnMeta;
import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.yaheen.c2c.base.gencode.ScanTable;
import com.yaheen.c2c.base.gencode.model.AppModelGenerator;
import com.yaheen.c2c.base.gencode.model.AppModelGeneratorConfig;
import io.jboot.Jboot;
import io.jboot.codegen.CodeGenHelpler;
import io.jboot.utils.ArrayUtil;
import io.jboot.utils.FileUtil;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * FileName: GenCode
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 自动生成c2c-app项目对应的Model类及Base类，并为Model类填上对应的注释
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class GenCode {

    public static void main(String[] args) throws Exception {
        AppModelGenerator.doGenerate();
        System.out.println("start generate entity attribute...");
        AppModelGeneratorConfig config = Jboot.config(AppModelGeneratorConfig.class);
        String modelPackage = config.getModelpackage();
        String modelDir = PathKit.getWebRootPath() + "/src/main/java/" + modelPackage.replace(".", "/");
        String removedTablenamePrefixes = config.getRemovedtablenameprefixes();
        if (StrKit.notBlank(removedTablenamePrefixes)) {
            removedTablenamePrefixes = StrKit.firstCharToUpperCase(removedTablenamePrefixes.replaceFirst("_", ""));
        }
        writeFinalAttrToModel(modelDir, removedTablenamePrefixes);
        System.out.println("entity attribute generate finished !!!");
    }

    /**
     * 往Model类自动加入数据库字段全局静态属性
     * 作用：避免程序到处手动写属性名
     *
     * @throws Exception
     */
    private static void writeFinalAttrToModel(String modelPath, String removedTablenamePrefixes) throws Exception {
        File modelFolder = new File(modelPath);
        if (modelFolder.isDirectory()) {
            File[] files = modelFolder.listFiles();
            if (ArrayUtil.isNotEmpty(files)) {
                Map<String, Map<String, String>> tableMap = ScanTable.getTableMap();
                StringBuilder sb = new StringBuilder();
                List<TableMeta> tableMetas = CodeGenHelpler.createMetaBuilder().build();
                for (TableMeta tableMeta : tableMetas) {
                    for (File file : files) {
                        String modelName = StrKit.notBlank(removedTablenamePrefixes) ? tableMeta.modelName.replaceFirst(removedTablenamePrefixes, "") : tableMeta.modelName;
                        if (file.getName().equals(modelName + ".java")) {
                            Map<String, String> columnMap = tableMap.get(tableMeta.name);
                            List<ColumnMeta> columns = tableMeta.columnMetas;
                            //获取文件原来内容
                            sb.append(FileUtil.readString(file));
                            //在文件{}里面追加内容
                            sb.delete(sb.lastIndexOf("}"), sb.length());
                            sb.append("    public static final String tableNameDes = \"表名\";\n");
                            sb.append("    public static final String tableName = \"").append(tableMeta.modelName.toLowerCase()).append("\";\n\n");
                            for (ColumnMeta column : columns) {
                                if (ArrayUtil.isNotEmpty(columnMap)) {
                                    String comment = columnMap.get(column.name);
                                    if (StrKit.notBlank(comment)) {
                                        sb.append("    public static final String ").append(column.attrName).append("Des = \"").append(comment).append("\";\n");
                                    }
                                }
                                sb.append("    public static final String ").append(column.attrName).append(" = \"").append(column.name).append("\";\n");
                            }
                            sb.append("\n}");
                            //采用覆盖方式重写文件
                            new FileWriter(file).write(sb.toString());
                            sb.setLength(0);
                        }
                    }
                }
            }
        }
    }

}