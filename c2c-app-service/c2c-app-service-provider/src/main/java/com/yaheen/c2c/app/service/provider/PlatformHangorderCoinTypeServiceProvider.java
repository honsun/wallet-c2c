package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformHangorderCoinTypeService;
import com.yaheen.c2c.app.service.entity.model.PlatformHangorderCoinType;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

import java.util.List;


@RPCBean
public class PlatformHangorderCoinTypeServiceProvider extends JbootServiceBase<PlatformHangorderCoinType> implements PlatformHangorderCoinTypeService {

    @Override
    public boolean exist(String platformId, String originalCoinId, String targetCoinId) {
        SqlPara sp = Db.getSqlPara("platformHangorderCoinType.exist");
        sp.addPara(platformId).addPara(originalCoinId).addPara(targetCoinId);
        return DAO.findFirst(sp).getLong("count") > 0;
    }

    @Override
    public List<PlatformHangorderCoinType> search(String platformId) {
        SqlPara sp = Db.getSqlPara("platformHangorderCoinType.get");
        sp.addPara(platformId);
        return DAO.find(sp);
    }
}