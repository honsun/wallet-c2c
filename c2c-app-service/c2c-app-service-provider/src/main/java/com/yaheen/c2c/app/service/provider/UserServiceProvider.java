package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.User;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;


@RPCBean
public class UserServiceProvider extends JbootServiceBase<User> implements UserService {

    @Override
    public User findByEmail(String email) {
        SqlPara sp = Db.getSqlPara("user.findByEmail");
        sp.addPara(email);
        return DAO.findFirst(sp);
    }

    @Override
    public User findByPlatformIdAndUserId(String platformId, String userId) {
        SqlPara sp = Db.getSqlPara("user.findByPlatformIdAndUserId");
        sp.addPara(platformId).addPara(userId);
        User user=DAO.findFirst(sp);
        return user;
    }

    @Override
    public boolean add(String platformId, String userId) {
        SqlPara sp = Db.getSqlPara("user.add");
        sp.addPara(platformId).addPara(userId);
        return Db.update(sp) > 0;

    }

}