package com.yaheen.c2c.app.service.provider;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformPrepayService;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import com.yaheen.c2c.app.service.entity.model.User;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

import java.util.List;


@RPCBean
public class PlatformPrepayServiceProvider extends JbootServiceBase<PlatformPrepay> implements PlatformPrepayService {

    @Override
    public List<PlatformPrepay> findByIdParentPlatformPrepayId(String parentPlatformPrepayId) {
        SqlPara sp = Db.getSqlPara("platformPrepay.findByPlatformId");
        sp.addPara(parentPlatformPrepayId);
        return DAO.find(sp);
    }

    @Override
    public Page<PlatformPrepay> search(PlatformPrepay prepay, Integer pageNum, Integer pageSize) {
        StringBuilder select = new StringBuilder();
        //预支付订单父类ID、预支付订单ID、平台ID、币种名称、币种数量、支付方名称、收款方名称、预支付订单类型、创建时间
        select.append("select pp.`platform_prepay_id`,pp.`parent_platform_prepay_id`,pp.`platform_id`,pc.`coin_name`,pp.`quantity`,ifnull(u.`nickname`,pf.`platform_name`) as payer_nickname,ifnull(tu.`nickname`,pf.`platform_name`) as rceiver_nickname,pp.`create_time`, ");
        select.append("( case pp.`type` ");
        select.append("when 1 then '缴纳保证金' when 2 then '退款保证金' when 3 then '锁仓挂单' when 4 then '取消挂单' when 5 then '关闭挂单' when 6 then '锁仓订单' when 7 then '订单交易' when 8 then '订单交易手续费' ");
        select.append("else null end) as type, ");
        select.append("( case pp.`status_id` ");
        select.append("when 1 then '未付款' when 2 then '付款成功' when 3 then '付款失败' when 4 then '已取消' when 5 then '已失效' else null end) as status_name ");
        StringBuilder query = new StringBuilder();
        query.append("from tb_platform_prepay pp ");
        query.append("left join tb_platform_coin pc on pc.`coin_id`=pp.`coin_id` ");
        query.append("left join tb_user u on u.`user_id`=pp.`payer_id` ");
        query.append("left join tb_user tu on tu.`user_id`=pp.`receiver_id` ");
        query.append("left join  tb_platform pf on pp.`platform_id`=pf.`platform_id` ");
//        query.append("left join  tb_platform_merchant pm1 on pp.`payer_id`=pm1.`merchant_id` ");
//        query.append("left join  tb_platform_merchant pm2 on pp.`receiver_id`=pm2.`merchant_id` ");
        query.append("where pp.`platform_id`='").append(prepay.getPlatformId()).append("' ");
        if (null != prepay.getStatusId()) {
            query.append("and pp.`status_id`=").append(prepay.getStatusId()).append(" ");
        }
        if (StrKit.notBlank(prepay.getCoinId())) {
            query.append("and pp.`coin_id`='").append(prepay.getCoinId()).append("' ");
        }
        query.append("and pp.`type` <> 8 ");
        query.append("and (pp.`payer_id` ='").append(prepay.getStr(User.userId).toString()).append("' or pp.`receiver_id`='").append(prepay.getStr(User.userId).toString()).append("') ");
        return DAO.paginate(pageNum, pageSize, select.toString(), query.toString());
    }

    @Override
    public PlatformPrepay get(String platformPrepayId) {
        SqlPara sp = Db.getSqlPara("platformPrepay.get");
        sp.addPara(platformPrepayId);
        return DAO.findFirst(sp);
    }

}