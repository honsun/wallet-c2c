package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformMerchantService;
import com.yaheen.c2c.app.service.entity.model.PlatformMerchant;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

import java.util.List;


@RPCBean
public class PlatformMerchantServiceProvider extends JbootServiceBase<PlatformMerchant> implements PlatformMerchantService {

    @Override
    public List<PlatformMerchant> getPlatformMerchant(String platformId,int type) {
        SqlPara sp = Db.getSqlPara("platformMerchant.getPlatformMerchant");
        sp.addPara(platformId).addPara(type);
        return DAO.find(sp);
    }
}