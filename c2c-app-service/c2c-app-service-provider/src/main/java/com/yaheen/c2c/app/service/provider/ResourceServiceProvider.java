package com.yaheen.c2c.app.service.provider;

import com.yaheen.c2c.app.service.ResourceService;
import com.yaheen.c2c.app.service.entity.model.Resource;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;


@RPCBean
public class ResourceServiceProvider extends JbootServiceBase<Resource> implements ResourceService {

}