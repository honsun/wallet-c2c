package com.yaheen.c2c.app.service.provider;

import cn.hutool.core.util.ObjectUtil;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.exception.HandlerException;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

@RPCBean
public class HangOrderServiceProvider extends JbootServiceBase<HangOrder> implements HangOrderService {

    @Override
    public Page<HangOrder> search(HangOrder search) {
        StringBuilder select = new StringBuilder();
        //挂单ID、挂单用户名称、原币种ID、原币种名称、原币种数量、目标币种ID、目标币种名称、目标币种数量、当时的币币汇率、挂单剩余可购买数、最少购买数、最大购买数、开始时间、状态名称、状态ID
        select.append("select ho.`hang_order_id`,u.`nickname`,ho.`price`,ho.`start_time`,ho.`original_coin_quantity`,");
        select.append("ho.`target_coin_quantity`,ho.`remain`,ho.`min_buy`,ho.`max_buy`,ho.`user_id`, ");
        select.append("ho.`original_coin_id`,pc1.`coin_name` as original_coin_name,ho.`target_coin_id`,pc2.`coin_name` as target_coin_name, ");
        select.append("( case ho.`status_id` ");
        select.append("when 1 then '未锁仓' when 2 then '成功锁仓' when 3 then '锁仓失败' when 4 then '完成' when 5 then '取消' when 6 then '失效' else null end) as status_name ");
        StringBuilder query = new StringBuilder();
        query.append("from tb_hang_order ho ");
        query.append("left join tb_user u on u.`user_id`=ho.`user_id` ");
        query.append("left join tb_platform_coin pc1 on pc1.`coin_id`=ho.`original_coin_id` ");
        query.append("left join tb_platform_coin pc2 on pc2.`coin_id`=ho.`target_coin_id` ");
        query.append("where ho.`platform_id`='").append(search.getPlatformId()).append("' ");
        if (ObjectUtil.isNotNull(search.get(HangOrder.targetCoinId))) {
            query.append("and ho.`original_coin_id`='").append(search.getTargetCoinId()).append("' ");
        }
        if(ObjectUtil.isNotNull(search.get(User.userId))){  //有userid则是查询用户个人，否则就是查询所有人
            query.append("and ho.`user_id`='").append(search.getUserId()).append("' ");
        }else{
            query.append("and ho.`status_id`=2 and ho.`user_id`<>'").append(search.getStr("currentUserId")).append("' ");
        }
        query.append("order by ho.`start_time` ");
        String pageNum = search.get(Constant.pageNum, "1");
        String pageSize = search.get(Constant.pageSize, "100");
        if (!pageNum.matches("[0-9]+") && !pageSize.matches("[0-9]+")) {
            throw new HandlerException("请输入正确的整型");
        }
        return DAO.paginate(Integer.parseInt(pageNum), Integer.parseInt(pageSize), select.toString(), query.toString());
    }

    @Override
    public boolean existUnfinishOrder(String hangOrderId) {
        SqlPara sp = Db.getSqlPara("hangorder.existUnfinishOrder");
        sp.addPara(hangOrderId);
        return DAO.findFirst(sp).getLong("count") > 0;
    }

    @Override
    public HangOrder get(String hangOrderId, String platformId) {
        SqlPara sp = Db.getSqlPara("hangorder.get");
        sp.addPara(hangOrderId).addPara(platformId).addPara(platformId);
        return DAO.findFirst(sp);
    }
}