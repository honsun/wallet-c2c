/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: GenCode
 * Author:   15820
 * Date:     2019/3/16 15:52
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.app.service.gencode;

import cn.hutool.core.io.file.FileWriter;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.yaheen.c2c.base.gencode.serviceimpl.AppServiceImplGenerator;
import com.yaheen.c2c.base.gencode.serviceimpl.AppServiceImplGeneratorConfig;
import io.jboot.Jboot;
import io.jboot.utils.ArrayUtil;
import io.jboot.utils.FileUtil;

import java.io.File;

/**
 * FileName: GenCode
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 自动根据数据库表和当前项目的Service类生成对应的ServiceProvider类
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class GenCode {

    public static void main(String[] args) throws Exception {
        AppServiceImplGenerator.doGenerate();
        System.out.println("start generate provider rpcbean...");
        AppServiceImplGeneratorConfig config = Jboot.config(AppServiceImplGeneratorConfig.class);
        String servicePackage = config.getServicepackage();
        String serviceDir = PathKit.getWebRootPath() + "/src/main/java/" + servicePackage.replace(".", "/") + "/provider";
        writeRpcBeanToProvider(serviceDir);
        System.out.println("provider rpcbean generate finished !!!");
    }

    /**
     * 往Model类自动加入数据库字段全局静态属性
     * 作用：避免程序到处手动写属性名
     *
     * @throws Exception
     */
    private static void writeRpcBeanToProvider(String servicePath) throws Exception {
        File modelFolder = new File(servicePath);
        if (modelFolder.isDirectory()) {
            File[] files = modelFolder.listFiles();
            if (ArrayUtil.isNotEmpty(files)) {
                for (File file : files) {
                    //获取文件原来内容
                    String fileContent = FileUtil.readString(file);
                    if (StrKit.notBlank(fileContent)) {
                        fileContent=fileContent.replaceAll("@Bean", "@RPCBean");
                        fileContent=fileContent.replaceAll("io.jboot.aop.annotation.Bean", "io.jboot.components.rpc.annotation.RPCBean");
                    }
                    //采用覆盖方式重写文件
                    new FileWriter(file).write(fileContent);
                }
            }
        }
    }
}