package com.yaheen.c2c.app.service.provider;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.OrderService;
import com.yaheen.c2c.app.service.PlatformCoinMarketService;
import com.yaheen.c2c.app.service.entity.model.*;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.util.IdUtil;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


@RPCBean
public class OrderServiceProvider extends JbootServiceBase<Order> implements OrderService {

    private Logger logger = LoggerFactory.getLogger(OrderServiceProvider.class);

    @RPCInject
    private HangOrderService hangOrderService;
    @RPCInject
    private PlatformCoinMarketService coinMarketService;

    @Override
    public boolean add(Platform platform, HangOrder hangOrder, Order orderA) {

        //根据订单A手动创建订单B
        String commonId = IdUtil.nextId();
        Date startTime = new Date();
        orderA.setOrderId(IdUtil.nextId());
        orderA.setCommonOrderId(commonId);
        orderA.setHangOrderId(hangOrder.getHangOrderId());
        orderA.setStartTime(startTime);

        Order orderB = new Order();
        orderB.setOrderId(IdUtil.nextId());
        orderB.setCommonOrderId(commonId);
        orderB.setUserId(hangOrder.getUserId());
        orderB.setHangOrderId(hangOrder.getHangOrderId());
        orderB.setStartTime(startTime);
        orderB.setOriginalCoinId(orderA.getTargetCoinId());
        orderB.setOriginalCoinQuantity(orderA.getTargetCoinQuantity());
        orderB.setTargetCoinId(orderA.getOriginalCoinId());
        //分别计算总手续费,平台手续费,c2c交易手续费
        BigDecimal totalChargeB = orderA.getOriginalCoinQuantity().multiply(platform.getTotalCharge());
        BigDecimal platformChargeB = orderA.getOriginalCoinQuantity().multiply(platform.getPlatformCharge());
        BigDecimal c2cChargeB = orderA.getOriginalCoinQuantity().multiply(platform.getC2cCharge());
        orderB.setTotalCharge(totalChargeB);
        orderB.setPlatformCharge(platformChargeB);
        orderB.setC2cCharge(c2cChargeB);
        orderB.setTargetCoinQuantity((orderA.getOriginalCoinQuantity().subtract(orderB.getTotalCharge())));

        boolean addResult = false;
        //同时保存两个订单
        if (orderA.save() && orderB.save()) {
            //订单保存成功后需要更新挂单的剩余可购买数,注意:::剩余可购买数必须大于目前订单购买的数量方可更新
            SqlPara sp = Db.getSqlPara("hangorder.updateHangOrderRemain");
            sp.addPara(hangOrder.getRemain().subtract(orderB.getOriginalCoinQuantity()).setScale(6, RoundingMode.DOWN)).addPara(hangOrder.getHangOrderId()).addPara(orderB.getOriginalCoinQuantity());
            addResult = Db.update(sp) > 0;
        }
        if (addResult) {
            PlatformMerchant managedAccount = platform.get("managedAccount");
            PlatformMerchant chargeAccount = platform.get("chargeAccount");
            //开始生成预支付订单
            String parentPlatformPrepayId = IdUtil.nextId();
            //预支单订单1:转coin到托管账号(锁仓)
            String platformPrepayId1 = IdUtil.nextId();
            PlatformPrepay prepay1 = new PlatformPrepay();
            prepay1.setPlatformPrepayId(platformPrepayId1);
            prepay1.setParentPlatformPrepayId(parentPlatformPrepayId);
            prepay1.setPlatformId(platform.getPlatformId());
            prepay1.setPayerId(orderA.getUserId());
            prepay1.setReceiverId(managedAccount.getMerchantId());
            prepay1.setCoinId(orderA.getOriginalCoinId());
            prepay1.setQuantity(orderA.getOriginalCoinQuantity());
            prepay1.setType(3);
            prepay1.setCreateTime(startTime);
            //预支单订单2:从托管账户转coin到订单用户
            String platformPrepayId2 = IdUtil.nextId();
            PlatformPrepay prepay2 = new PlatformPrepay();
            prepay2.setPlatformPrepayId(platformPrepayId2);
            prepay2.setParentPlatformPrepayId(parentPlatformPrepayId);
            prepay2.setPlatformId(platform.getPlatformId());
            prepay2.setPayerId(managedAccount.getMerchantId());
            prepay2.setReceiverId(orderA.getUserId());
            prepay2.setCoinId(orderA.getTargetCoinId());
            prepay2.setQuantity(orderA.getTargetCoinQuantity());
            prepay2.setType(7);
            prepay2.setCreateTime(startTime);
            //预支单订单3:从托管账户转coin到挂单用户
            String platformPrepayId3 = IdUtil.nextId();
            PlatformPrepay prepay3 = new PlatformPrepay();
            prepay3.setPlatformPrepayId(platformPrepayId3);
            prepay3.setParentPlatformPrepayId(parentPlatformPrepayId);
            prepay3.setPlatformId(platform.getPlatformId());
            prepay3.setPayerId(managedAccount.getMerchantId());
            prepay3.setReceiverId(orderB.getUserId());
            prepay3.setCoinId(orderB.getTargetCoinId());
            prepay3.setQuantity(orderB.getTargetCoinQuantity());
            prepay3.setType(7);
            prepay3.setCreateTime(startTime);
            //预支单订单4:从托管账户转coin到手续费账号
            String platformPrepayId4 = IdUtil.nextId();
            PlatformPrepay prepay4 = new PlatformPrepay();
            prepay4.setPlatformPrepayId(platformPrepayId4);
            prepay4.setParentPlatformPrepayId(parentPlatformPrepayId);
            prepay4.setPlatformId(platform.getPlatformId());
            prepay4.setPayerId(managedAccount.getMerchantId());
            prepay4.setReceiverId(chargeAccount.getMerchantId());
            prepay4.setType(8);
            prepay4.setCreateTime(startTime);
            prepay4.setCoinId(platform.getMarginCoinId());
            prepay4.setQuantity(orderB.getTotalCharge());
            //所有预支付订单必须全部添加成功才算成功
            if (!prepay1.save() || !prepay2.save() || !prepay3.save() || !prepay4.save()) {
                addResult = false;
            }

            //通知平台也要生成对应的预支付订单数据
            Map<String, String> params = new HashMap<>();
            //平台参数
            params.put(Platform.platformId, platform.getPlatformId());
            params.put(Platform.accessToken, platform.getAccessToken());
            //预支付订单参数,可能多条
            List<Map<String, String>> prepays = new ArrayList<>();

            Map<String, String> prepayParams1 = new HashMap<>();
            prepayParams1.put(PlatformPrepay.parentPlatformPrepayId, parentPlatformPrepayId);
            prepayParams1.put(PlatformPrepay.platformPrepayId, platformPrepayId1);
            prepayParams1.put(PlatformPrepay.payerId, orderA.getUserId());
            prepayParams1.put(PlatformPrepay.receiverId, managedAccount.getMerchantId());
            prepayParams1.put(PlatformPrepay.coinId, orderA.getOriginalCoinId());
            prepayParams1.put(PlatformPrepay.quantity, orderA.getOriginalCoinQuantity().toString());
            prepays.add(prepayParams1);

            Map<String, String> prepayParams2 = new HashMap<>();
            prepayParams2.put(PlatformPrepay.parentPlatformPrepayId, parentPlatformPrepayId);
            prepayParams2.put(PlatformPrepay.platformPrepayId, platformPrepayId2);
            prepayParams2.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
            prepayParams2.put(PlatformPrepay.receiverId, orderA.getUserId());
            prepayParams2.put(PlatformPrepay.coinId, orderA.getTargetCoinId());
            prepayParams2.put(PlatformPrepay.quantity, orderA.getTargetCoinQuantity().toString());
            prepays.add(prepayParams2);

            Map<String, String> prepayParams3 = new HashMap<>();
            prepayParams3.put(PlatformPrepay.parentPlatformPrepayId, parentPlatformPrepayId);
            prepayParams3.put(PlatformPrepay.platformPrepayId, platformPrepayId3);
            prepayParams3.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
            prepayParams3.put(PlatformPrepay.receiverId, orderB.getUserId());
            prepayParams3.put(PlatformPrepay.coinId, orderB.getTargetCoinId());
            prepayParams3.put(PlatformPrepay.quantity, orderB.getTargetCoinQuantity().toString());
            prepays.add(prepayParams3);

            Map<String, String> prepayParams4 = new HashMap<>();
            prepayParams4.put(PlatformPrepay.parentPlatformPrepayId, parentPlatformPrepayId);
            prepayParams4.put(PlatformPrepay.platformPrepayId, platformPrepayId4);
            prepayParams4.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
            prepayParams4.put(PlatformPrepay.receiverId, chargeAccount.getMerchantId());
            prepayParams4.put(PlatformPrepay.coinId, platform.getMarginCoinId());
            prepayParams4.put(PlatformPrepay.quantity, prepay4.getQuantity().toString());
            prepays.add(prepayParams4);

            params.put("prepays", JsonKit.toJson(prepays));
//            String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
//            boolean createResult = false;  //平台生成预支付订单的结果
//            if (StrKit.notBlank(jsonResult)) {
//                JSONObject jsonObject = JSONObject.parseObject(jsonResult);
//                if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
//                    //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
//                    if (parentPlatformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
//                        createResult = true;
//                    }
//                }
//            }
//            if (!createResult) {
//                addResult = false;
//                //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
//                prepay1.delete();
//                prepay2.delete();
//                prepay3.delete();
//                prepay4.delete();
//                orderA.delete();
//                orderB.delete();
//                //还原挂单的可购买数量
//                SqlPara sp = Db.getSqlPara("hangOrder.updateHangOrderRemain");
//                sp.addPara(hangOrder.getRemain().add(orderB.getOriginalCoinQuantity())).addPara(hangOrder.getHangOrderId()).addPara(hangOrder.getRemain());
//                Db.update(sp);
//                logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
//                throw new HandlerException("创建平台预支付订单过程发生错误，请重试");
//            }
            orderA.setPlatformPrepayId(parentPlatformPrepayId);
            orderB.setPlatformPrepayId(parentPlatformPrepayId);
            if (!orderA.update() || !orderB.update()) {
                addResult = false;
                logger.error("更新订单对应的预支付订单信息失败");
                throw new HandlerException("更新订单对应的预支付订单信息失败");
            }
        }
        return addResult;
    }


    @Override
    public Page<Order> search(String userId, String coinId, Integer statusId, Integer pageNum, Integer pageSize) {
        StringBuilder select = new StringBuilder();
        //订单ID、订单开始时间、原币种ID、原币种名称、目标币种ID、目标币种名称、原币种数量、目标币种数量、订单状态、订单取消时间、订单完成时间、币币行情价格、公共订单ID
        select.append("select o.`order_id`,o.`hang_order_id`,o.`start_time`,o.`common_order_id`,o.`original_coin_id`,o.`original_coin_quantity`,o.`target_coin_id`,o.`target_coin_quantity`,");
        select.append("o.`status_id`,o.`finish_time`,o.`cancel_time`,o_pc.`coin_name` as origin_coin_name,t_pc.`coin_name` as target_coin_name,u.nickname,ho.`price` ");

        StringBuilder query = new StringBuilder();
        query.append("from tb_order o ");
        query.append("left join tb_hang_order ho on ho.`hang_order_id`=o.`hang_order_id` ");
        query.append("left join tb_platform_coin o_pc on o_pc.`coin_id`=o.`original_coin_id` ");
        query.append("left join tb_platform_coin t_pc on t_pc.`coin_id`=o.`target_coin_id` ");
        query.append("left join tb_user u on u.`user_id`=ho.`user_id` ");
        query.append("where o.user_id='").append(userId).append("' ");
        if (StrKit.notBlank(coinId)) {
            query.append("and (o.`original_coin_id`='").append(coinId).append("' or o.`target_coin_id`='").append(coinId).append("') ");
        }
        if (statusId > 0) {
            query.append("and o.status_id=").append(statusId).append(" ");
        }
        query.append("order by ").append(statusId > 0 ? "o.`status_id`=" + statusId + " desc," : "").append("o.status_id=1 desc,o.`start_time` desc ");

        return DAO.paginate(pageNum, pageSize, select.toString(), query.toString());
    }

    @Override
    public boolean cancel(Order orderA) {
        Columns columns = Columns.create();
        columns.eq(Order.commonOrderId, orderA.getCommonOrderId());
        columns.ne(Order.orderId, orderA.getOrderId());
        Order orderB = DAO.findFirstByColumns(columns);
        if (null == orderB) {
            return false;
        }
        orderA.setCancelTime(new Date());
        orderA.setStatusId(11);
        orderB.setCancelTime(new Date());
        orderB.setStatusId(11);
        if (orderA.update() && orderB.update()) {
            HangOrder hangOrder = hangOrderService.findById(orderA.getHangOrderId());
            return hangOrder.update();
        }
        return false;
    }


    @Override
    public boolean confirm(Order orderA) {
        Columns columns = Columns.create();
        columns.eq(Order.commonOrderId, orderA.getCommonOrderId());
        columns.ne(Order.orderId, orderA.getOrderId());
        Order orderB = DAO.findFirstByColumns(columns);
        if (null == orderB) {
            return false;
        }
        orderA.setStatusId(10);
        orderB.setStatusId(10);
        return orderA.update() && orderB.update();
    }

    @Override
    public Order get(String orderId, String platformId) {
        SqlPara sp = Db.getSqlPara("order.get");
        sp.addPara(orderId).addPara(platformId).addPara(platformId);
        return DAO.findFirst(sp);
    }
}