package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformMerchantService;
import com.yaheen.c2c.app.service.PlatformService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformMerchant;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;


@RPCBean
public class PlatformServiceProvider extends JbootServiceBase<Platform> implements PlatformService {

    @RPCInject
    private PlatformMerchantService merchantService;

    @Override
    public Platform findWithDetail(String platformId) {
        SqlPara sq = Db.getSqlPara("platform.findWithDetail");
        Platform platform = DAO.findFirst(sq.addPara(platformId));
        if (null != platform) {
            Columns columns1 = Columns.create();
            columns1.eq(PlatformMerchant.platformId, platform.getPlatformId());
            columns1.ne(PlatformMerchant.type, 1);
            //托管账号
            platform.put("managedAccount", merchantService.paginateByColumns(1, Integer.MAX_VALUE, columns1).getList().get(0));
            Columns columns2 = Columns.create();
            columns2.eq(PlatformMerchant.platformId, platform.getPlatformId());
            columns2.ne(PlatformMerchant.type, 2);
            //手续费账号
            platform.put("chargeAccount", merchantService.paginateByColumns(1, Integer.MAX_VALUE, columns2).getList().get(0));
        }
        return platform;
    }
}