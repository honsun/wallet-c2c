/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: Application
 * Author:   15820
 * Date:     2019/3/16 22:05
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.app.service;

import io.jboot.app.JbootApplication;

/**
 * FileName: Application
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 生产者服务入口
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class AppServiceProviderApplication {

    public static void main(String[] args) {
        JbootApplication.run(args);
    }
}