package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformCoinService;
import com.yaheen.c2c.app.service.entity.model.PlatformCoin;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

import java.util.List;


@RPCBean
public class PlatformCoinServiceProvider extends JbootServiceBase<PlatformCoin> implements PlatformCoinService {

    @Override
    public boolean synPlatformCoins(String platformId, List<PlatformCoin> coins) {
        StringBuilder sb = new StringBuilder();
        sb.append("delete ");
        sb.append("from tb_platform_coin ");
        sb.append("where `platform_id`=? and `coin_id` not in(");
        for (int i = 0, size = coins.size(); i < size; i++) {
            sb.append("'").append(coins.get(i).getCoinId()).append("'");
            sb.append(i + 1 != size ? "," : "");
        }
        sb.append(");");
        Db.update(sb.toString(), platformId);
        sb.setLength(0);
        sb.append("insert into tb_platform_coin(`platform_id`,`coin_id`,`coin_name`,`description`) values ");
        for (int i = 0, size = coins.size(); i < size; i++) {
            sb.append("('").append(platformId).append("','").append(coins.get(i).getCoinId()).append("','");
            sb.append(coins.get(i).getCoinName()).append("','").append(coins.get(i).getDescription()).append("')");
            sb.append(i + 1 != size ? "," : "");
        }
        sb.append(" on duplicate key update `create_time`=now();");
        return Db.update(sb.toString()) > 0;
    }

    @Override
    public List<PlatformCoin> get(String platformId){
        SqlPara sp = Db.getSqlPara("platformCoin.getPlatformCoin");
        sp.addPara(platformId);
        return DAO.find(sp);
    }
}