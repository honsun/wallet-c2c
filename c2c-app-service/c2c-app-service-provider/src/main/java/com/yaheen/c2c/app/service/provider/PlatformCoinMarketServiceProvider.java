package com.yaheen.c2c.app.service.provider;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yaheen.c2c.app.service.PlatformCoinMarketService;
import com.yaheen.c2c.app.service.entity.model.PlatformCoinMarket;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.service.JbootServiceBase;

import java.util.List;


@RPCBean
public class PlatformCoinMarketServiceProvider extends JbootServiceBase<PlatformCoinMarket> implements PlatformCoinMarketService {
    @Override
    public PlatformCoinMarket get(String platformId, String originalCoinId, String targeCoinId) {
        SqlPara sp = Db.getSqlPara("platformCoinMarket.get");
        sp.addPara(platformId).addPara(originalCoinId).addPara(targeCoinId);
        return DAO.findFirst(sp);
    }

    @Override
    public List<PlatformCoinMarket> getTradeType(String platformId,String targetCoinId) {
        SqlPara sp = Db.getSqlPara("platformCoinMarket.PlatformCoinMarket");
        sp.addPara(platformId).addPara(targetCoinId);
        return DAO.find(sp);
    }

}