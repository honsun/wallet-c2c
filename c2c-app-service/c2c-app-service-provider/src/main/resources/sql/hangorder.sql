#sql("existUnfinishOrder")
  select count(*) as count
  from tb_order
  where `hang_order_id`=? and `status_id`=1
#end

#sql("updateHangOrderRemain")
  update tb_hang_order
  set `remain`=?
  where `hang_order_id`=? and `remain`>=?
#end

#sql("get")
  /**挂单用户名称、原币种ID、原币种名称、目标币种ID、**/
  select u.`nickname`,pc.`coin_name` as original_coin,ho.`original_coin_id`,ho.`original_coin_quantity`,
  c.`coin_name` as target_coin,ho.`target_coin_id`,ho.`target_coin_quantity`, ho.`price`,
  ho.`min_buy`,ho.`max_buy`,ho.`remark`,ho.`remain`,ho.`start_time`,
  ho.`finish_time`,ho.`cancel_time`,ho.`status_id`,ho.`user_id`,(case ho.`status_id`
			when 1 then '未锁仓'
			when 2 then '成功锁仓'
      when 3 then '锁仓失败'
      when 4 then '完成'
      when 5 then '取消'
      when 6 then '已失效'
			else null
		end) as status_name
  from tb_hang_order ho
  left join tb_user u on ho.`user_id`=u.`user_id`
  left join tb_platform_coin pc on ho.`original_coin_id`=pc.`coin_id`
  left join tb_platform_coin c on ho.`target_coin_id`=c.`coin_id`
  where ho.`hang_order_id`=? and pc.`platform_id`=? and c.`platform_id`=?;
#end