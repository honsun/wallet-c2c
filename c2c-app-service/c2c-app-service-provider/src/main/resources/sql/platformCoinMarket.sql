#sql("get")
  select *
  from tb_platform_coin_market
  where `platform_id`=? and `original_coin_id`=? and `target_coin_id`=?;
#end

#sql("getTradeType")
  select distinct pcm.original_coin_id,pcm.target_coin_id,pc1.coin_name as original_coin_name,pc2.coin_name as target_coin_name
  from tb_platform_coin_market pcm
  left join tb_platform_coin pc1 on pc1.`coin_id`=pcm.`original_coin_id`
  left join tb_platform_coin pc2 on pc2.`coin_id`=pcm.`target_coin_id`
  where pcm.`platform_id`=? and pcm.`target_coin_id`=?
#end
