#sql("exist")
  select count(*) as count
  from tb_platform_hangorder_coin_type
  where `platform_id`=? and `original_coin_id`=? and `target_coin_id`=?;
#end

#sql("get")
  select phct.`platform_hangorder_coin_type_id`, phct.`platform_id`,phct.`original_coin_id`,pc1.`coin_name` as original_coin_name,
  phct.`target_coin_id`,pc2.`coin_name` as target_coin_name,phct.`status_id`,phct.`min_buy`,phct.`max_buy`,phct.`create_time`
  from tb_platform_hangorder_coin_type phct
  left join tb_platform_coin pc1 on pc1.`coin_id`=phct.`original_coin_id`
  left join tb_platform_coin pc2 on pc2.`coin_id`=phct.`target_coin_id`
  where phct.`platform_id`=? and phct.`status_id`=1;
#end
