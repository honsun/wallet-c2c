#sql("findByIdParentPlatformPrepayId")
  select *
  from tb_platform_prepay
  where `parent_platform_prepay_id`=?
#end

#sql("get")
  select pp.`platform_prepay_id`,pp.`parent_platform_prepay_id`,pp.`platform_id`,pc.`coin_name`,pp.`quantity`,u.`nickname` as payer_nickname,tu.`nickname` as rceiver_nickname,pp.`create_time`,(case pp.`type`
			when 1 then '缴纳保证金'
			when 2 then '退款保证金'
      when 3 then '锁仓挂单'
      when 4 then '取消挂单'
      when 5 then '关闭挂单'
      when 6 then '锁仓订单'
      when 7 then '订单交易'
      when 8 then '订单交易手续费'
			else null
		end) as type,pp.`status_id`,(case pp.`status_id`
			when 1 then '未付款'
			when 2 then '付款成功'
      when 3 then '付款失败'
      when 4 then '已取消'
      when 5 then '已失效'
			else null
		end) as status_name
  from tb_platform_prepay pp
  left join tb_platform_coin  pc on pc.`coin_id`=pp.`coin_id`
  left join  tb_user  u on u.`user_id`=pp.`payer_id`
  left join  tb_user  tu on tu.`user_id`=pp.`receiver_id`
  where pp.`platform_prepay_id`=?;
#end