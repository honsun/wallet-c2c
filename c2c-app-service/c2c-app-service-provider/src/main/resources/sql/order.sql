#sql("get")
  select o.`order_id`,o.`hang_order_id`,u.`nickname` as payer_nickname,tu.`nickname` as receiver_nickname, pc.`coin_name` as original_coin,
    o.`original_coin_quantity`,o.`target_coin_quantity`,c.`coin_name` as target_coin,o.`total_charge`,o.`start_time`,o.`finish_time`,
    o.`cancel_time`,ho.`price`,o.`status_id`,(case o.`status_id`
			when 1 then '未付款'
			when 2 then '付款成功'
      when 3 then '付款失败'
      when 4 then '已取消'
      when 5 then '已失效'
			else null
		end) as status_name
  from tb_order o
  left join tb_hang_order ho on o.`hang_order_id`=ho.`hang_order_id`
  left join tb_user u on o.`user_id`=u.`user_id`
  left join tb_user tu on ho.`user_id`=tu.`user_id`
  left join tb_platform_coin pc on o.`original_coin_id`=pc.`coin_id`
  left join tb_platform_coin c on o.`target_coin_id`=c.`coin_id`
  where o.`order_id`=? and pc.`platform_id`=? and c.`platform_id`=?;
#end
