#namespace("user")
  #include("user.sql")
#end

#namespace("hangorder")
  #include("hangorder.sql")
#end

#namespace("order")
  #include("order.sql")
#end

#namespace("platform")
  #include("platform.sql")
#end

#namespace("platformCoinMarket")
  #include("platformCoinMarket.sql")
#end

#namespace("platformMerchant")
  #include("platformMerchant.sql")
#end

#namespace("platformCoin")
  #include("platformCoin.sql")
#end

#namespace("platformPrepay")
  #include("platformPrepay.sql")
#end
#namespace("platformHangorderCoinType")
  #include("platformHangorderCoinType.sql")
#end

#namespace("platformOrderCoinType")
  #include("platformOrderCoinType.sql")
#end