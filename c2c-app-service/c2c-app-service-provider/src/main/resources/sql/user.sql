#sql("findByEmail")
  select *
  from tb_user
  where `email` = ?
#end

#sql("findByPlatformIdAndUserId")
  select *
  from tb_user
  where `platform_id`=? and `user_id` = ?
#end

#sql("add")
  insert into tb_user(`platform_id`,`user_id`)
  value (?,?)
  on duplicate key update `register_time`=now()
#end
