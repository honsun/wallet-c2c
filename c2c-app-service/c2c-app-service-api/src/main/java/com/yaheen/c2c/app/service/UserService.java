package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.User;
import io.jboot.db.model.Columns;

import java.util.List;

public interface UserService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public User findById(Object id);


    /**
     * find all model
     *
     * @return all <User
     */
    public List<User> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(User model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(User model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(User model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(User model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<User> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<User> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<User> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 根据邮箱查询用户信息
     * @param email　邮箱
     * @return
     */
    User findByEmail(String email);

    /**
     * 根据平台ID及用户ID查询用户信息
     * @param platformId
     * @param userId
     * @return
     */
    User findByPlatformIdAndUserId(String platformId, String userId);

    /**
     * 新增用户信息
     * @param platformId
     * @param userId
     * @return
     */
    boolean add(String platformId, String userId);

}