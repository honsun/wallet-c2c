package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.PlatformHangorderCoinType;
import io.jboot.db.model.Columns;

import java.util.List;

public interface PlatformHangorderCoinTypeService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public PlatformHangorderCoinType findById(Object id);


    /**
     * find all model
     *
     * @return all <PlatformHangorderCoinType
     */
    public List<PlatformHangorderCoinType> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(PlatformHangorderCoinType model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(PlatformHangorderCoinType model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(PlatformHangorderCoinType model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(PlatformHangorderCoinType model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<PlatformHangorderCoinType> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<PlatformHangorderCoinType> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<PlatformHangorderCoinType> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 判断平台是否需要该币币交易的挂单
     * @param platformId
     * @param originalCoinId
     * @param targetCoinId
     * @return
     */
    boolean exist(String platformId, String originalCoinId, String targetCoinId);

    /**
     * 获取平台挂单支持的交易对
     * @param platformId
     * @return
     */
    List<PlatformHangorderCoinType> search(String platformId);
}