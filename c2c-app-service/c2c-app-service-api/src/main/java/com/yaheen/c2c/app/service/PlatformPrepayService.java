package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import io.jboot.db.model.Columns;

import java.util.List;

public interface PlatformPrepayService {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public PlatformPrepay findById(Object id);


    /**
     * find all model
     *
     * @return all <PlatformPrepay
     */
    public List<PlatformPrepay> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(PlatformPrepay model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(PlatformPrepay model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(PlatformPrepay model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(PlatformPrepay model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<PlatformPrepay> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<PlatformPrepay> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<PlatformPrepay> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 查询预支付订单信息
     *
     * @param platformId
     * @return
     */
    List<PlatformPrepay> findByIdParentPlatformPrepayId(String parentPlatformPrepayId);

    public Page<PlatformPrepay> search(PlatformPrepay prepay, Integer pageNum, Integer pageSize);

    public PlatformPrepay get(String platformPrepayId);

}