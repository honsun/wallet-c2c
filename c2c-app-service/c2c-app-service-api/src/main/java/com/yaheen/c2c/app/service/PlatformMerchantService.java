package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.PlatformMerchant;
import io.jboot.db.model.Columns;

import java.util.List;

public interface PlatformMerchantService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public PlatformMerchant findById(Object id);


    /**
     * find all model
     *
     * @return all <PlatformMerchant
     */
    public List<PlatformMerchant> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(PlatformMerchant model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(PlatformMerchant model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(PlatformMerchant model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(PlatformMerchant model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<PlatformMerchant> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<PlatformMerchant> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<PlatformMerchant> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);


    /**
     * 根据平台ID获取平台商户账号
     * @param platformId
     * @return
     */
    public List<PlatformMerchant> getPlatformMerchant(String platformId,int type);
}