package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.Platform;
import io.jboot.db.model.Columns;

import java.util.List;

public interface PlatformService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Platform findById(Object id);


    /**
     * find all model
     *
     * @return all <Platform
     */
    public List<Platform> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Platform model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Platform model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Platform model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Platform model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Platform> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Platform> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Platform> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 查询所有平台详细信息(包括币种信息)
     * @return
     */
    public Platform findWithDetail(String platformId);


}