package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import com.yaheen.c2c.app.service.entity.model.Order;
import com.yaheen.c2c.app.service.entity.model.Platform;
import io.jboot.db.model.Columns;
import io.jboot.db.model.Or;

import java.util.List;

public interface OrderService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Order findById(Object id);


    /**
     * find all model
     *
     * @return all <Order
     */
    public List<Order> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Order model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Order model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Order model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Order model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Order> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Order> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Order> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 添加订单
     * @param order
     * @return
     */
    public boolean add(Platform platform,HangOrder hangOrder, Order order);

    /**
     * 分页搜索订单信息
     * @param statusId
     * @param anInt
     * @param anInt1
     * @return
     */
    Page<Order> search(String userId,String coinId,Integer statusId, Integer pageNum, Integer pageSize);

    /**
     * 取消订单信息
     * @param dbOrder
     * @return
     */
    boolean cancel(Order dbOrder);

    /**
     * 确认付款订单信息
     * @param orderId
     * @return
     */
    boolean confirm(Order dbOrder);

    public Order get(String orderId, String platformId);
}