package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import io.jboot.db.model.Columns;

import java.util.List;

public interface HangOrderService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public HangOrder findById(Object id);


    /**
     * find all model
     *
     * @return all <HangOrder
     */
    public List<HangOrder> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(HangOrder model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(HangOrder model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(HangOrder model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(HangOrder model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<HangOrder> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<HangOrder> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<HangOrder> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);


    /**
     * 分页搜索挂单信息
     * @return
     */
    Page<HangOrder> search(HangOrder search);

    /**
     * 判断当前挂单是否存在未完成的订单
     * @param hangOrderId
     * @return
     */
    boolean existUnfinishOrder(String hangOrderId);

    public HangOrder get(String hangOrderId, String platformId);
}