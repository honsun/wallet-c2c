package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.PlatformCoinMarket;
import io.jboot.db.model.Columns;

import java.math.BigDecimal;
import java.util.List;

public interface PlatformCoinMarketService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public PlatformCoinMarket findById(Object id);


    /**
     * find all model
     *
     * @return all <PlatformCoinMarket
     */
    public List<PlatformCoinMarket> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(PlatformCoinMarket model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(PlatformCoinMarket model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(PlatformCoinMarket model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(PlatformCoinMarket model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<PlatformCoinMarket> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<PlatformCoinMarket> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<PlatformCoinMarket> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 获取平台货币行情信息
     * @param platformId
     * @param originalCoinId
     * @param targeCoinId
     * @return
     */
    public  PlatformCoinMarket get(String platformId,String originalCoinId,String targeCoinId);

    /**
     * 获取平台的交易对类型
     * @param platformId
     * @return
     */
    List<PlatformCoinMarket> getTradeType(String platformId,String targetCoinId);

}