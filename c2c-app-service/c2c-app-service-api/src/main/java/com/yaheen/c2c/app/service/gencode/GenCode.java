/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: GenCode
 * Author:   15820
 * Date:     2019/3/16 15:44
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.yaheen.c2c.app.service.gencode;

import com.yaheen.c2c.base.gencode.service.AppServiceGenerator;

/**
 * FileName: GenCode
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 自动生成c2c-app项目对应的Service类
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class GenCode {
    public static void main(String[] args) {
        AppServiceGenerator.doGenerate();
    }
}