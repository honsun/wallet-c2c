package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.PlatformCoin;
import io.jboot.db.model.Columns;

import java.util.List;

public interface PlatformCoinService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public PlatformCoin findById(Object id);


    /**
     * find all model
     *
     * @return all <PlatformCoin
     */
    public List<PlatformCoin> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(PlatformCoin model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(PlatformCoin model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(PlatformCoin model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(PlatformCoin model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<PlatformCoin> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<PlatformCoin> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<PlatformCoin> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    /**
     * 同步平台货币信息
     * @param platformId 平台ID
     * @param coins 货币信息列表
     * @return
     */
    boolean synPlatformCoins(String platformId, List<PlatformCoin> coins);

    public List<PlatformCoin> get(String platformId);
}