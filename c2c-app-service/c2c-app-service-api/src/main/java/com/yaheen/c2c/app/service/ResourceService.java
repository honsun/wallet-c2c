package com.yaheen.c2c.app.service;

import com.jfinal.plugin.activerecord.Page;
import com.yaheen.c2c.app.service.entity.model.Resource;
import io.jboot.db.model.Columns;

import java.util.List;

public interface ResourceService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Resource findById(Object id);


    /**
     * find all model
     *
     * @return all <Resource
     */
    public List<Resource> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Resource model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Resource model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Resource model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Resource model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Resource> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Resource> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Resource> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);


}