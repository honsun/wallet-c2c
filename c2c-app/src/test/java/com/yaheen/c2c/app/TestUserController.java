package com.yaheen.c2c.app;

import cn.hutool.json.JSONUtil;
import com.jfinal.aop.Aop;
import com.jfinal.kit.JsonKit;
import com.jfinal.server.undertow.UndertowServer;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.User;
import io.jboot.Jboot;
import io.jboot.app.JbootApplication;
import org.databene.contiperf.PerfTest;
import org.databene.contiperf.Required;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

/**
 * FileName: TestUserController
 * Author:   Honsun
 * Date:     2019/5/10 14:43
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/5/10 14:43     2.0.0
 */


public class TestUserController {

//    @Rule
//    public ContiPerfRule i = new ContiPerfRule();

    @Before
    public void run() {
        JbootApplication.setBootArg("jboot.rpc.type", "dubbo");
        JbootApplication.setBootArg("jboot.rpc.registryType", "zookeeper");
        JbootApplication.setBootArg("jboot.rpc.registryAddress", "127.0.0.1:2181");
        JbootApplication.run(null);
        System.out.println("开始启动服务");
    }

//    @PerfTest(invocations = 1000, threads = 10)
//    @Required(max = 1200, average = 250, totalTime = 60000)
    @Test
    public void testUserservice() {
        UserService userService = Jboot.service(UserService.class);
        System.out.println(JsonKit.toJson(userService.findAll()));
        List<User> users=userService.findAll();
        for(User user:users){
            System.out.println(user.toJson());
        }
    }
}
