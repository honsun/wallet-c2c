package com.yaheen.c2c.app.ContiPerfDemo;

import junit.framework.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.notification.RunNotifier;
import io.jboot.app.JbootApplication;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

/**
 * FileName: MyRunner
 * Author:   huangfu
 * Date:     2019/5/13 10:53
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * huangfu         2019/5/13 10:53     2.0.0
 */
public class MyRunner extends BlockJUnit4ClassRunner {

    public MyRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }
    @Override
    public void run(RunNotifier notifier) {
        JbootApplication.setBootArg("jboot.datasource.type", "mysql");
        JbootApplication.setBootArg("jboot.datasource.driver", "com.mysql.jdbc.Driver");
        JbootApplication.setBootArg("jboot.datasource.url", "jdbc:mysql://127.0.0.1:3306/wallet-c2c?useUnicode=true&amp;characterEncoding=utf-8&amp;useSSL=false");
        JbootApplication.setBootArg("jboot.datasource.user", "root");
        JbootApplication.setBootArg("jboot.datasource.password", "123456");
        JbootApplication.setBootArg("undertow.port", "8888");
        JbootApplication.setBootArg("jboot.rpc.type", "dubbo");
        JbootApplication.setBootArg("jboot.rpc.registryType", "zookeeper");
        JbootApplication.setBootArg("jboot.rpc.registryAddress", "127.0.0.1:2181");
        JbootApplication.run(null);
        System.out.println("开始启动服务。");
        super.run(notifier);
    }
}