package com.yaheen.c2c.app;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.base.gencode.ScanTable;
import io.jboot.Jboot;
import io.jboot.app.JbootApplication;
import io.jboot.utils.ArrayUtil;
import io.jboot.utils.ClassScanner;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;

/**
 * FileName: ScanResource
 * Author:   Honsun
 * Date:     2019/4/16 14:34
 * Description: 扫描项目所有资源并插入到数据库
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/4/16 14:34     2.0.0
 */
public class ScanResource {

    public static void main(String[] args) {
        Map<String, String> uriMap = new HashMap<>();
        List<Class> classes = ClassScanner.scanClassByAnnotation(RequestMapping.class, true);
        if (ArrayUtil.isNotEmpty(classes)) {
            for (Class clazz : classes) {
                RequestMapping requestMapping = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
                String uri = requestMapping.value();
                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    String fullUri = uri + "/" + method.getName();
                    String operationDes = "";  //注解内容可能为空
                    //读取方法的描述注解
                    Annotation apiOperation = method.getAnnotation(ApiOperation.class);
                    if (null != apiOperation) {
                        operationDes = ((ApiOperation) apiOperation).value();
                        Extension[] extensions = ((ApiOperation) apiOperation).extensions();
                        if (ArrayUtil.isNotEmpty(extensions)) {
                            for (Extension extension : extensions) {
                                for (ExtensionProperty property : extension.properties()) {
                                    System.out.println(property.name() + "   " + property.value());
                                }
                            }
                        }
                    }

                    uriMap.put(fullUri, operationDes);
                }
            }
        }
        //如果获取到数据，则需要更新到数据库
        if (!uriMap.isEmpty()) {
            //先删除所有数据，再插入最新数据
            StringBuilder sb = new StringBuilder();
            sb.append("delete from tb_resource;");
            update(sb.toString());
            sb.setLength(0);
            sb.append("insert into tb_resource(uri,description,create_time) values ");
            int i = 0;
            for (String uri : uriMap.keySet()) {
                ++i;
                sb.append("('").append(uri).append("','").append(uriMap.get(uri)).append("',now())");
                sb.append(i == uriMap.size() ? "" : ",");
            }
            update(sb.toString());
        }
    }

    /**
     * 获取数据库下的所有表名
     */
    public static void update(String sql) {
        List<String> tableNames = new ArrayList<>();
        Connection conn = ScanTable.getConnection();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                if (null != ps) {
                    ps.close();
                }
                ScanTable.closeConnection(conn);
            } catch (SQLException e) {
            }
        }
    }
}
