package com.yaheen.c2c.app.ContiPerfDemo;

import com.jfinal.aop.Aop;
import com.jfinal.kit.JsonKit;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import io.jboot.Jboot;
import io.jboot.app.JbootApplication;
import io.jboot.components.rpc.annotation.RPCInject;
import org.databene.contiperf.PerfTest;
import org.databene.contiperf.Required;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;


/**
 * FileName: ContiPerfTest
 * Author:   huangfu
 * Date:     2019/5/10 10:46
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * huangfu         2019/5/10 10:46     2.0.0
 * 测试报告查看方式：打开target/contiperf-report/ 目录下的index.html即可查看测试结果数据
 */
@RunWith(MyRunner.class)//使用自定义运行器，启动JbootApplication应用
public class ContiPerfTest {
    @Rule
    public ContiPerfRule i = new ContiPerfRule();  // 激活ContiPerf工具

    @Test
    @PerfTest(invocations = 10, threads = 2)// threads = 20并发执行20个线程,  invocations = 300每个线程执行300次，和线程数量无关
    @Required(max = 1200, average = 250, totalTime = 60000) // //指定每次执行的最长时间/平均时间/总时间
    public void test1() {
        HangOrderService hangOrderService = Jboot.service(HangOrderService.class);
        List<HangOrder> hangOrder =hangOrderService.findAll();
        for (HangOrder entity:hangOrder) {
            System.out.println( entity.toJson());
        }
        System.out.println(JsonKit.toJson(hangOrder));

    }


}