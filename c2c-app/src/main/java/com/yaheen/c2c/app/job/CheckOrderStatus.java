package com.yaheen.c2c.app.job;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import com.yaheen.c2c.app.service.entity.model.Order;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * FileName: CheckOrderStatus
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 检查订单状态,超过30分钟未付款则直接取消该订单
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/1 * * * *")
@EnableDistributedRunnable
public class CheckOrderStatus implements Runnable {

    private Logger logger = LoggerFactory.getLogger(CheckOrderStatus.class);

    private boolean doing = false; //如果正在处理，则不进行下一次处理

    @Override
    public void run() {
        if (!doing) {
            doing = true;
            //查询所有未付款且超过30分钟的订单
            List<Record> records = Db.find(Db.getSqlPara("job.findTimeoutOrders"));
            if (ArrayUtil.isNotEmpty(records)) {
                StringBuilder sb = new StringBuilder();
                for (Record record : records) {
                    Db.tx(new IAtom() {
                        @Override
                        public boolean run() throws SQLException {
                            String platformPrepayId = record.getStr(Order.platformPrepayId);
                            sb.append("update tb_platform_prepay set `status_id`=5 where `parent_platform_prepay_id`=?");
                            if (Db.update(sb.toString(), platformPrepayId) == 0) {
                                return false;
                            }
                            sb.setLength(0);
                            sb.append("update tb_order set `status_id`=5 where `common_order_id`=?");
                            if (Db.update(sb.toString(), record.getStr(Order.commonOrderId)) == 0) {
                                return false;
                            }
                            //返回订单的可购买数量到挂单上
                            sb.append("update tb_hang_order ho,tb_order o ");
                            sb.append("set ho.`remain`=ho.`remain`+o.`target_coin_id`*(o.`target_coin_quantity`+o.`total_charge`) ");
                            sb.append("where ho.`hang_order_id`=o.`hang_order_id` ");
                            sb.append("and ho.`original_coin_id`=o.`target_coin_id` and ho.`target_coin_id`=o.`original_coin_id` ");
                            sb.append("and o.`platform_prepay_id`=? ");
                            if (Db.update(sb.toString(), record.getStr(Order.platformPrepayId)) == 0) {
                                return false;
                            }
                            return true;
                        }
                    });
                }
            }
        }
    }
}
