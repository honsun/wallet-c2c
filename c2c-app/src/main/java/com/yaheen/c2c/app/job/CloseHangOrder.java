package com.yaheen.c2c.app.job;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformMerchant;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import com.yaheen.c2c.base.util.IdUtil;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FileName: CloseHangOrder
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 挂单剩余可购买数少于最少购买数则自动取消
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@Cron("*/1 * * * *")
@EnableDistributedRunnable
public class CloseHangOrder implements Runnable {

    private Logger logger = LoggerFactory.getLogger(CloseHangOrder.class);

    @RPCInject
    private HangOrderService hangOrderService;

    @Override
    public void run() {

        List<Record> records = Db.find(Db.getSqlPara("job.findBelowStandardHangOrders"));
        List<Record> lastRecords = new ArrayList<Record>();
        if (ArrayUtil.isNotEmpty(records)) {
            for (Record record : records) {
                if (!hangOrderService.existUnfinishOrder(record.getStr(HangOrder.hangOrderId))) {
                    //record.set(HangOrder.statusId, 5);
                    lastRecords.add(record);
                }
            }
        }

        if (ArrayUtil.isNotEmpty(lastRecords)) {
            for (Record record : lastRecords) {
                Db.tx(new IAtom() {
                    @Override
                    public boolean run() throws SQLException {

                        Platform platform = Jboot.getRedis().get(record.getStr(HangOrder.platformId));
                        if (null == platform || StrKit.isBlank(platform.getPrepayUrl())) {
                            return false;
                        }
                        PlatformMerchant managedAccount = platform.get("managedAccount");
                        if (null == managedAccount) {
                            return false;
                        }
                        /**
                         * 如果是由平台创建预支付订单ID，如果我们这边没有保存成功，而平台用户又付款了，这时会导致资金流向不明
                         * 所以由我们创建预支付订单ID
                         */
                        String platformPrepayId = IdUtil.nextId();
                        //创建预支付订单数据
                        PlatformPrepay prepay = new PlatformPrepay();
                        prepay.setPlatformPrepayId(platformPrepayId);
                        prepay.setParentPlatformPrepayId(platformPrepayId);
                        prepay.setPlatformId(platform.getPlatformId());
                        prepay.setPayerId(managedAccount.getMerchantId());
                        prepay.setReceiverId(record.getStr(HangOrder.userId));
                        prepay.setCoinId(record.getStr(HangOrder.originalCoinId));
                        prepay.setQuantity(record.getBigDecimal(HangOrder.remain));
                        prepay.setType(5);
                        prepay.setStatusId(1);
                        if (!prepay.save()) {
                            return false;
                        }
                        //我们成功生成预支付订单数据则通知平台也要生成对应的预支付订单数据
                        Map<String, String> params = new HashMap<>();
                        //平台参数
                        params.put(Platform.platformId, platform.getPlatformId());
                        params.put(Platform.accessToken, platform.getAccessToken());
                        //预支付订单参数,可能多条
                        List<Map<String, String>> prepays = new ArrayList<>();
                        Map<String, String> prepayParams = new HashMap<>();

                        prepayParams.put(PlatformPrepay.parentPlatformPrepayId, platformPrepayId);
                        prepayParams.put(PlatformPrepay.platformPrepayId, platformPrepayId);
                        prepayParams.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
                        prepayParams.put(PlatformPrepay.receiverId, record.getStr(HangOrder.userId));
                        prepayParams.put(PlatformPrepay.coinId, record.getStr(HangOrder.originalCoinId));
                        prepayParams.put(PlatformPrepay.quantity, record.getBigDecimal(HangOrder.remain).toString());

                        prepays.add(prepayParams);
                        params.put("prepays", JsonKit.toJson(prepays));

                        String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
                        boolean createResult = false;  //平台生成预支付订单的结果
                        if (StrKit.notBlank(jsonResult)) {
                            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
                            if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
                                //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
                                if (platformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
                                    createResult = true;
                                }
                            }
                        }
                        if (!createResult) {
                            //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
                            prepay.delete();
                            logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
                            return false;
                        }
                        record.set(HangOrder.refundPlatformPrepayId, platformPrepayId);
                        if (!Db.update("tb_hang_order", record)) {
                            return false;
                        }
                        return true;
                    }
                });
            }
        }

    }
}
