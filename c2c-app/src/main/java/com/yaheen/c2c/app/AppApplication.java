package com.yaheen.c2c.app;

import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.core.Const;
import com.jfinal.json.JFinalJsonFactory;
import com.jfinal.plugin.activerecord.tx.TxByMethodRegex;
import com.jfinal.render.ViewType;
import com.yaheen.c2c.base.interceptor.ExceptionInterceptor;
import com.yaheen.c2c.base.interceptor.OriginInteceptor;
import io.jboot.app.JbootApplication;
import io.jboot.core.listener.JbootAppListenerBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FileName: Application
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 服务启动入口
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/18 17:12     2.0.0
 */
public class AppApplication extends JbootAppListenerBase {

    private Logger logger = LoggerFactory.getLogger(AppApplication.class);

    public static void main(String[] args) {
        //程序主入口
        JbootApplication.run(args);
    }

    @Override
    public void onConstantConfig(Constants constants) {
        super.onConstantConfig(constants);
        //解决JSON内容返回前端变成驼峰
        constants.setJsonFactory(new JFinalJsonFactory());
        constants.setViewType(ViewType.JSP);
        //设置上传文件的最大限制
        constants.setMaxPostSize(100 * Const.DEFAULT_MAX_POST_SIZE);
    }

    @Override
    public void onInterceptorConfig(Interceptors interceptors) {
        //跨域过滤器
        interceptors.add(new OriginInteceptor());
        //声明式事务实现方式为拦截器
        interceptors.add(new TxByMethodRegex("(.*add.*|.*save.*|.*update.*|.*delete.*|.*cancel.*|.*confirm.*)"));
        //异常处理过滤器
        interceptors.add(new ExceptionInterceptor());
    }

    @Override
    public void onStart() {
        super.onStart();
        logger.info(">>>>>>程序启动成功!");
    }

}

