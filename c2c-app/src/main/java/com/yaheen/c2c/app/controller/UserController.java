package com.yaheen.c2c.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.PlatformService;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.*;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.common.RedisKey;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.util.IdUtil;
import com.yaheen.c2c.base.validator.LongValidator;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FileName: UserController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 用户服务管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/user")
@Api(tags = "用户服务")
public class UserController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @RPCInject
    private PlatformService platformService;
    @RPCInject
    private UserService userService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "注入用户信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })})
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.platformId),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.userIdDes),
            @ApiImplicitParam(name = User.accessToken, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.accessTokenDes),
            @ApiImplicitParam(name = User.tokenTimeout, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.tokenTimeoutDes),
    })
    @Before({PostRequest.class})
    public void inject() throws Exception {
        User user = jsonToModel(User.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(user.getPlatformId(), new StringValidator(User.platformIdDes))
                .on(user.getUserId(), new StringValidator(User.userIdDes))
                .on(user.getAccessToken(), new StringValidator(User.accessTokenDes))
                .on(user.getTokenTimeout(), new LongValidator(User.tokenTimeoutDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        //平台ID存在代表我们已对接该平台
        if (!redis.exists(user.getPlatformId())) {
            throw new HandlerException("该平台信息不存在");
        }
        User dbUser = userService.findByPlatformIdAndUserId(user.getPlatformId(), user.getUserId());
        if (null == dbUser) {
            throw new HandlerException("该用户信息不存在");
        }
        dbUser.setAccessToken(user.getAccessToken());
        dbUser.setTokenTimeout(user.getTokenTimeout());
        if (!dbUser.update()) {
            throw new HandlerException("注入用户信息失败，请重试");
        }
        //放入redis用于异步向平台申请用户信息
        Jboot.getRedis().lpush(RedisKey.CACHE_GET_USERINFO, dbUser.toJson());
        SuccessResult();
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "缴纳保证金",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521")})
    @Before({PostRequest.class, AuthInteceptor.class})
    public void payMargin() throws Exception {
        User dbUser = redis.get(currentPlatformId() + "-" + currentUserId());
        //获取平台数据及商户的托管账号
        Platform platform = redis.get(currentPlatformId());
        if (StrKit.isBlank(platform.getPrepayUrl())) {
            throw new HandlerException("平台数据为空");
        }
        PlatformMerchant managedAccount = platform.get("managedAccount");
        if (null == managedAccount) {
            throw new HandlerException("商户数据为空");
        }

        /**
         * 如果是由平台创建预支付订单ID，如果我们这边没有保存成功，而平台用户又付款了，这时会导致资金流向不明
         * 所以由我们创建预支付订单ID
         */
        String platformPrepayId = IdUtil.nextId();

        //创建预支付订单数据
        PlatformPrepay prepay = new PlatformPrepay();
        prepay.setPlatformPrepayId(platformPrepayId);
        prepay.setParentPlatformPrepayId(platformPrepayId);
        prepay.setPlatformId(platform.getPlatformId());
        prepay.setPayerId(currentUserId());
        prepay.setReceiverId(managedAccount.getMerchantId());
        prepay.setCoinId(platform.getMarginCoinId());
        prepay.setQuantity(platform.getMarginQuantity());
        prepay.setType(1);
        prepay.setStatusId(1);
        if (!prepay.save()) {
            throw new HandlerException("创建保证金订单过程发生错误，请重试");
        }
        //我们成功生成预支付订单数据则通知平台也要生成对应的预支付订单数据
        Map<String, String> params = new HashMap<>();
        //平台参数
        params.put(Platform.platformId, platform.getPlatformId());
        params.put(Platform.accessToken, platform.getAccessToken());
        //预支付订单参数,可能多条
        List<Map<String, String>> prepays = new ArrayList<>();

        Map<String, String> prepayParams = new HashMap<>();
        prepayParams.put(PlatformPrepay.parentPlatformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.platformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.payerId, currentUserId());
        prepayParams.put(PlatformPrepay.receiverId, managedAccount.getMerchantId());
        prepayParams.put(PlatformPrepay.coinId, platform.getMarginCoinId());
        prepayParams.put(PlatformPrepay.quantity, platform.getMarginQuantity().toString());

        prepays.add(prepayParams);
        params.put("prepays", JsonKit.toJson(prepays));
        String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
        boolean createResult = false;  //平台生成预支付订单的结果
        if (StrKit.notBlank(jsonResult)) {
            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
            if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
                //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
                if (platformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
                    createResult = true;
                }
            }
        }
        if (!createResult) {
            //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
            prepay.delete();
            logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
            throw new HandlerException("创建缴纳保证金订单过程发生错误，请重试");
        }
        SuccessResult();
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "退款保证金",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521")})
    @Before({PostRequest.class, AuthInteceptor.class})
    public void refundMargin() throws Exception {
        User dbUser = redis.get(currentPlatformId() + "-" + currentUserId());
        //获取平台数据及商户的托管账号
        Platform platform = redis.get(currentPlatformId());
        if (StrKit.isBlank(platform.getPrepayUrl())) {
            throw new HandlerException("平台数据为空");
        }
        PlatformMerchant managedAccount = platform.get("managedAccount");
        if (null == managedAccount) {
            throw new HandlerException("商户数据为空");
        }

        /**
         * 如果是由平台创建预支付订单ID，如果我们这边没有保存成功，而平台用户又付款了，这时会导致资金流向不明
         * 所以由我们创建预支付订单ID
         */
        String platformPrepayId = IdUtil.nextId();

        //创建预支付订单数据
        PlatformPrepay prepay = new PlatformPrepay();
        prepay.setPlatformPrepayId(platformPrepayId);
        prepay.setParentPlatformPrepayId(platformPrepayId);
        prepay.setPlatformId(platform.getPlatformId());
        prepay.setPayerId(currentUserId());
        prepay.setReceiverId(managedAccount.getMerchantId());
        prepay.setCoinId(platform.getMarginCoinId());
        prepay.setQuantity(platform.getMarginQuantity());
        prepay.setType(2);
        prepay.setStatusId(1);
        if (!prepay.save()) {
            throw new HandlerException("创建退款保证金订单过程发生错误，请重试");
        }
        //我们成功生成预支付订单数据则通知平台也要生成对应的预支付订单数据
        Map<String, String> params = new HashMap<>();
        //平台参数
        params.put(Platform.platformId, platform.getPlatformId());
        params.put(Platform.accessToken, platform.getAccessToken());
        //预支付订单参数,可能多条
        List<Map<String, String>> prepays = new ArrayList<>();

        Map<String, String> prepayParams = new HashMap<>();
        prepayParams.put(PlatformPrepay.parentPlatformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.platformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
        prepayParams.put(PlatformPrepay.receiverId, currentUserId());
        prepayParams.put(PlatformPrepay.coinId, platform.getMarginCoinId());
        prepayParams.put(PlatformPrepay.quantity, platform.getMarginQuantity().toString());

        prepays.add(prepayParams);
        params.put("prepays", JsonKit.toJson(prepays));
        String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
        boolean createResult = false;  //平台生成预支付订单的结果
        if (StrKit.notBlank(jsonResult)) {
            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
            if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
                //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
                if (platformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
                    createResult = true;
                }
            }
        }
        if (!createResult) {
            //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
            prepay.delete();
            logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
            throw new HandlerException("创建保证金订单过程发生错误，请重试");
        }
        SuccessResult();

    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取用户详情",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521")})
    @Before({PostRequest.class, AuthInteceptor.class})
    public void get() throws Exception {
        User dbUser = redis.get(currentPlatformId() + "-" + currentUserId());
        dbUser.remove(User.accessToken);
        SuccessResult(dbUser);
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "判断用户是否缴纳保证金",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521")})
    @Before({PostRequest.class, AuthInteceptor.class})
    public void checkMargin() throws Exception {
        User dbUser = redis.get(currentPlatformId() + "-" + currentUserId());
        SuccessResult(dbUser.getMargin().compareTo(Constant.zero) > 0);
    }

}