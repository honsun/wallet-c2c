package com.yaheen.c2c.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.PlatformCoinMarketService;
import com.yaheen.c2c.app.service.PlatformHangorderCoinTypeService;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.*;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.exception.NotFoundException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.util.IdUtil;
import com.yaheen.c2c.base.validator.IdValidator;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FileName: HangOrderController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 挂单服务管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/hangOrder")
@Api(tags = "挂单服务")
@Before(AuthInteceptor.class)
public class HangOrderController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(HangOrderController.class);

    @RPCInject
    private HangOrderService hangOrderService;
    @RPCInject
    private UserService userService;
    @RPCInject
    private PlatformCoinMarketService platformCoinMarketService;
    @RPCInject
    private PlatformHangorderCoinTypeService hangorderCoinTypeService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "搜索挂单",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = Constant.pageNum, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageNumDes, defaultValue = "1"),
            @ApiImplicitParam(name = Constant.pageSize, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageSizeDes, defaultValue = "10"),
            @ApiImplicitParam(name = HangOrder.targetCoinId, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = HangOrder.targetCoinIdDes),
            @ApiImplicitParam(name = HangOrder.userId, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = HangOrder.userIdDes)
    })
    @Before({PostRequest.class})
    public void search() throws Exception {
        HangOrder hangOrder = jsonToModel(HangOrder.class, true, 1);
        hangOrder.setPlatformId(currentPlatformId());
        hangOrder.put("currentUserId", currentUserId());
        SuccessResult(hangOrderService.search(hangOrder));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取平台支持的挂单交易对",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })})
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),
    })
    @Before({PostRequest.class})
    public void getTradeType() throws Exception {
        SuccessResult(hangorderCoinTypeService.search(currentPlatformId()));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "查询挂单详情",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = HangOrder.hangOrderId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = HangOrder.hangOrderIdDes)
    })
    @Before({PostRequest.class})
    public void get() throws Exception {
        HangOrder hangOrder = jsonToModel(HangOrder.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(hangOrder.getHangOrderId(), new StringValidator(HangOrder.hangOrderIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        hangOrder.setPlatformId(currentPlatformId());
        SuccessResult(hangOrderService.get(hangOrder.getHangOrderId(), hangOrder.getPlatformId()));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "添加挂单",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = HangOrder.originalCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.originalCoinIdDes),
            @ApiImplicitParam(name = HangOrder.originalCoinQuantity, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.originalCoinQuantityDes),
            @ApiImplicitParam(name = HangOrder.targetCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.targetCoinIdDes),
            @ApiImplicitParam(name = HangOrder.targetCoinQuantity, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.targetCoinQuantityDes),
            @ApiImplicitParam(name = HangOrder.price, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.priceDes),
            @ApiImplicitParam(name = HangOrder.minBuy, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.minBuyDes),
            @ApiImplicitParam(name = HangOrder.maxBuy, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.maxBuyDes),
            @ApiImplicitParam(name = HangOrder.remark, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = HangOrder.remarkDes)
    })
    @Before({PostRequest.class, AuthInteceptor.class})
    public void add() throws Exception {

        HangOrder hangOrder = jsonToModel(HangOrder.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(hangOrder.getOriginalCoinId(), new StringValidator(HangOrder.originalCoinIdDes))
                .on(hangOrder.getTargetCoinId(), new StringValidator(HangOrder.targetCoinIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        String platformId = currentPlatformId();
        Platform platform = redis.get(platformId);
        hangOrder.setUserId(currentUserId());
        hangOrder.setPlatformId(currentPlatformId());
        User dbUser = redis.get(currentPlatformId() + "-" + currentUserId());
        if (dbUser.getMargin().compareTo(Constant.zero) != 1) {
            throw new HandlerException("该用户未缴纳保证金，请先缴纳保证金");
        }
        if (!hangorderCoinTypeService.exist(hangOrder.getPlatformId(), hangOrder.getOriginalCoinId(), hangOrder.getTargetCoinId())) {
            throw new HandlerException("非法的币币交易对");
        }
        if (!StrKit.notBlank(hangOrder.getStr(HangOrder.originalCoinQuantity), hangOrder.getStr(HangOrder.targetCoinQuantity), hangOrder.getStr(HangOrder.price), hangOrder.getStr(HangOrder.minBuy), hangOrder.getStr(HangOrder.maxBuy))) {
            throw new HandlerException("原币种数量/目标币种数量/最少购买数/最多购买数/价格有误");
        }

        BigDecimal originalCoinQuantity = new BigDecimal(hangOrder.getStr(HangOrder.originalCoinQuantity)).setScale(6, BigDecimal.ROUND_DOWN);
        BigDecimal targetCoinQuantity = new BigDecimal(hangOrder.getStr(HangOrder.targetCoinQuantity)).setScale(6, BigDecimal.ROUND_DOWN);
        BigDecimal minBuy = new BigDecimal(hangOrder.getStr(HangOrder.minBuy)).setScale(6, BigDecimal.ROUND_DOWN);
        BigDecimal maxBuy = new BigDecimal(hangOrder.getStr(HangOrder.maxBuy)).setScale(6, BigDecimal.ROUND_DOWN);
        BigDecimal price = new BigDecimal(hangOrder.getStr(HangOrder.price)).setScale(6, BigDecimal.ROUND_DOWN);

        if (originalCoinQuantity.compareTo(Constant.zero) <= 0 || targetCoinQuantity.compareTo(Constant.zero) <= 0 ||
                minBuy.compareTo(Constant.zero) <= 0 || maxBuy.compareTo(Constant.zero) <= 0 || price.compareTo(Constant.zero) <= 0) {
            throw new HandlerException("原币种数量/目标币种数量/最少购买数/最多购买数/价格必須大于零");
        }
        if (0 == platform.getSplitHangorder()) {
            hangOrder.setMinBuy(originalCoinQuantity);
            hangOrder.setMaxBuy(originalCoinQuantity);
        } else {
            if (-1 == originalCoinQuantity.compareTo(minBuy) || 1 == originalCoinQuantity.compareTo(maxBuy)) {
                throw new HandlerException("数量必须在最少购买数与最大购买数之间");
            }
        }
        //获取平台数据及商户的托管账号
        if (StrKit.isBlank(platform.getPrepayUrl())) {
            throw new HandlerException("该平台无法生成支付订单，请联系管理员");
        }
        PlatformMerchant managedAccount = platform.get("managedAccount");
        if (null == managedAccount) {
            throw new HandlerException("该平台的托管账号有误，请联系管理员");
        }
        hangOrder.setHangOrderId(IdUtil.nextId());
        hangOrder.setOriginalCoinQuantity(originalCoinQuantity);
        hangOrder.setTargetCoinQuantity(targetCoinQuantity);
        hangOrder.setMinBuy(minBuy);
        hangOrder.setMaxBuy(maxBuy);
        hangOrder.setPrice(price);
        hangOrder.setRemain(originalCoinQuantity);
        hangOrder.setStatusId(1);

        /**
         * 如果是由平台创建预支付订单ID，如果我们这边没有保存成功，而平台用户又付款了，这时会导致资金流向不明
         * 所以由我们创建预支付订单ID
         */
        String platformPrepayId = IdUtil.nextId();
        //创建预支付订单数据
        PlatformPrepay prepay = new PlatformPrepay();
        prepay.setPlatformPrepayId(platformPrepayId);
        prepay.setParentPlatformPrepayId(platformPrepayId);
        prepay.setPlatformId(platform.getPlatformId());
        prepay.setPayerId(hangOrder.getUserId());
        prepay.setReceiverId(managedAccount.getMerchantId());
        prepay.setCoinId(hangOrder.getOriginalCoinId());
        prepay.setQuantity(hangOrder.getOriginalCoinQuantity());
        prepay.setType(3);
        prepay.setStatusId(1);
        if (!prepay.save()) {
            throw new HandlerException("创建预支付挂单过程发生错误，请重试");
        }
        //第一步：保存挂单信息到数据库
        hangOrder.setLockPlatformPrepayId(platformPrepayId);
        if (!hangOrder.save()) {
            throw new HandlerException("创建挂单过程发生错误，请重试");
        }
        //我们成功生成预支付订单数据则通知平台也要生成对应的预支付订单数据
        Map<String, String> params = new HashMap<>();
        //平台参数
        params.put(Platform.platformId, platform.getPlatformId());
        params.put(Platform.accessToken, platform.getAccessToken());
        //预支付订单参数,可能多条
        List<Map<String, String>> prepays = new ArrayList<>();
        Map<String, String> prepayParams = new HashMap<>();

        prepayParams.put(PlatformPrepay.parentPlatformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.platformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.payerId, hangOrder.getUserId());
        prepayParams.put(PlatformPrepay.receiverId, managedAccount.getMerchantId());
        prepayParams.put(PlatformPrepay.coinId, hangOrder.getOriginalCoinId());
        prepayParams.put(PlatformPrepay.quantity, hangOrder.getOriginalCoinQuantity().toString());

        prepays.add(prepayParams);
        params.put("prepays", JsonKit.toJson(prepays));

//        String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
//        boolean createResult = false;  //平台生成预支付订单的结果
//        if (StrKit.notBlank(jsonResult)) {
//            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
//            if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
//                //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
//                if (platformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
//                    createResult = true;
//                }
//            }
//        }
//        if (!createResult) {
//            //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
//            prepay.delete();
//            hangOrder.delete();
//            logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
//            throw new HandlerException("创建挂单过程发生错误，请重试");
//        }
        SuccessResult();
    }


    @ApiOperation(httpMethod = HttpMethods.POST, value = "取消挂单",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = HangOrder.hangOrderId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = HangOrder.hangOrderIdDes)
    })
    @Before({PostRequest.class})
    public void cancel() throws Exception {
        HangOrder order = jsonToModel(HangOrder.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(order.getHangOrderId(), new IdValidator(HangOrder.hangOrderIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        HangOrder dbHangOrder = hangOrderService.findById(order.getHangOrderId());
        //先判断挂单信息是否存在
        if (null == dbHangOrder) {
            throw new NotFoundException();
        }
        //再判断是否挂单用户
        if (!currentUserId().equalsIgnoreCase(dbHangOrder.getUserId())) {
            throw new HandlerException(Constant.NOT_PERMISSION_MESSAGE);
        }

        if (dbHangOrder.getStatusId().equals(4)) {
            throw new HandlerException("该挂单已完成，无法取消");
        }
        if (dbHangOrder.getStatusId().equals(5)) {
            throw new HandlerException("该挂单已取消，无需重复取消");
        }
        //如果当前挂单存在未完成的订单则无法取消
        if (hangOrderService.existUnfinishOrder(order.getHangOrderId())) {
            throw new HandlerException("当前挂单存在未完成的订单，无法取消");
        }
        //取消挂单的时候可能这条挂单已经被检测到低于标准而自动取消了所以为了避免重复取消要判断挂单的退款预支付订单号
        if (!StrKit.notBlank(dbHangOrder.getRefundPlatformPrepayId())) {
            throw new HandlerException("当前挂单正在取消，无需重复取消");
        }
        Platform platform = redis.get(dbHangOrder.getPlatformId());
        if (null == platform || StrKit.isBlank(platform.getPrepayUrl())) {
            throw new HandlerException("平台数据为空");
        }
        PlatformMerchant managedAccount = platform.get("managedAccount");
        if (null == managedAccount) {
            throw new HandlerException("商户数据为空");
        }
        /**
         * 如果是由平台创建预支付订单ID，如果我们这边没有保存成功，而平台用户又付款了，这时会导致资金流向不明
         * 所以由我们创建预支付订单ID
         */
        String platformPrepayId = IdUtil.nextId();
        //创建预支付订单数据
        PlatformPrepay prepay = new PlatformPrepay();
        prepay.setPlatformPrepayId(platformPrepayId);
        prepay.setParentPlatformPrepayId(platformPrepayId);
        prepay.setPlatformId(platform.getPlatformId());
        prepay.setPayerId(managedAccount.getMerchantId());
        prepay.setReceiverId(dbHangOrder.getUserId());
        prepay.setCoinId(dbHangOrder.getOriginalCoinId());
        prepay.setQuantity(dbHangOrder.getOriginalCoinQuantity());
        prepay.setType(4);
        prepay.setStatusId(1);
        if (!prepay.save()) {
            throw new HandlerException("创建预支付挂单过程发生错误，请重试");
        }
        //我们成功生成预支付订单数据则通知平台也要生成对应的预支付订单数据
        Map<String, String> params = new HashMap<>();
        //平台参数
        params.put(Platform.platformId, platform.getPlatformId());
        params.put(Platform.accessToken, platform.getAccessToken());
        //预支付订单参数,可能多条
        List<Map<String, String>> prepays = new ArrayList<>();
        Map<String, String> prepayParams = new HashMap<>();

        prepayParams.put(PlatformPrepay.parentPlatformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.platformPrepayId, platformPrepayId);
        prepayParams.put(PlatformPrepay.payerId, managedAccount.getMerchantId());
        prepayParams.put(PlatformPrepay.receiverId, dbHangOrder.getUserId());
        prepayParams.put(PlatformPrepay.coinId, dbHangOrder.getOriginalCoinId());
        prepayParams.put(PlatformPrepay.quantity, dbHangOrder.getOriginalCoinQuantity().toString());

        prepays.add(prepayParams);
        params.put("prepays", JsonKit.toJson(prepays));

        String jsonResult = HttpKit.post(platform.getPrepayUrl(), params, null);
        boolean createResult = false;  //平台生成预支付订单的结果
        if (StrKit.notBlank(jsonResult)) {
            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
            if (null != jsonObject && jsonObject.containsKey("code") && 200 == jsonObject.getIntValue("code")) {
                //如果返回的预支付订单ID就是我们提供的则代表平台成功生成预支付订单
                if (platformPrepayId.equalsIgnoreCase(jsonObject.getString(PlatformPrepay.parentPlatformPrepayId))) {
                    createResult = true;
                }
            }
        }
        if (!createResult) {
            //如果平台生成预支付订单失败，那我们这边也要把刚才创建的预支付订单数据删除
            prepay.delete();
            logger.error("创建平台预支付订单:{}过程发生错误:{}", JsonKit.toJson(params), jsonResult);
            throw new HandlerException("取消挂单的过程中发生错误，请重试");
        }
        dbHangOrder.set(HangOrder.refundPlatformPrepayId, platformPrepayId);
        if (!hangOrderService.update(dbHangOrder)) {
            throw new HandlerException("取消挂单的过程中发生错误，请重试");
        }
        SuccessResult();
    }

}