package com.yaheen.c2c.app.job;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Record;
import com.yaheen.c2c.app.service.entity.model.HangOrder;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

/**
 * FileName: CheckOrderStatus
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 检测挂单状态
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/1 * * * *")
@EnableDistributedRunnable
public class CheckHangOrderStatus implements Runnable {

    private Logger logger = LoggerFactory.getLogger(CheckHangOrderStatus.class);

    @Override
    public void run() {
        List<Record> records = Db.find(Db.getSqlPara("job.findTimeoutHangOrders"));
        if (ArrayUtil.isNotEmpty(records)) {
            for (Record record : records) {
                record.set(HangOrder.statusId, 7);
            }
            Db.tx(new IAtom() {
                @Override
                public boolean run() throws SQLException {
                    if (Db.update("tb_hang_order", records, records.size()) == 0) {
                        return false;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("update tb_platform_prepay set `status_id`=5 ");
                    sb.append("where `parent_platform_prepay_id` in(");
                    for (int i = 0, size = records.size(); i < size; i++) {
                        sb.append("'").append(records.get(i).getStr(HangOrder.lockPlatformPrepayId)).append("'");
                        sb.append(i + 1 != size ? "," : "");
                    }
                    sb.append(")");
                    if (Db.update(sb.toString()) == 0) {
                        return false;
                    }
                    return true;
                }
            });

        }
    }

}
