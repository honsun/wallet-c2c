package com.yaheen.c2c.app.job;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.service.PlatformService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformCoinMarket;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.util.IdUtil;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FileName: SynPlatformCoinMarket
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 每隔5分钟自动同步平台币种行情数据到数据库
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/5 * * * *")
@EnableDistributedRunnable
public class SynPlatformCoinMarket implements Runnable {

    private Logger logger = LoggerFactory.getLogger(SynPlatformCoinMarket.class);

    @RPCInject
    private PlatformService platformService;

    @Override
    public void run() {
        //从数据库获取最新的平台数据
        List<Platform> platforms = platformService.findAll();
        if (ArrayUtil.isNotEmpty(platforms)) {
            platforms.stream().forEach(platform -> {
                //平台同步货币行情的链接
                if (platform.getReferCoinMarket() == 1 && StrKit.notBlank(platform.getSynCoinMarketUrl())) {
                    //请求的参数
                    Map<String, String> params = new HashMap<>();
                    params.put(Platform.platformId, platform.getPlatformId());
                    params.put(Platform.accessToken, platform.getAccessToken());

                    boolean synResult = false;
                    StringBuilder sb = new StringBuilder();
                    String jsonResult = "";
                    //尝试5次不成功则报错
                    for (int i = 0; i < 5; i++) {
                        jsonResult = HttpKit.get(platform.getSynCoinMarketUrl(), params);
                        if (StrKit.notBlank(jsonResult)) {
                            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
                            //假设平台返回的data就是我们想要的数组
                            if (null != jsonObject && jsonObject.containsKey("data")) {
                                Object data = jsonObject.get("data");
                                //data必须是数组格式
                                if (data instanceof JSONArray) {
                                    JSONArray coinArrays = (JSONArray) data;
                                    if (!coinArrays.isEmpty() && coinArrays.size() > 0) {
                                        sb.append("insert into tb_platform_coin_market(`platform_coin_market_id`,`platform_id`,`original_coin_id`,`target_coin_id`,`price`) values ");
                                        for (int j = 0, size = coinArrays.size(); j < size; j++) {
                                            String originalCoinId = coinArrays.getJSONObject(i).getString(PlatformCoinMarket.originalCoinId);
                                            String targetCoinId = coinArrays.getJSONObject(i).getString(PlatformCoinMarket.targetCoinId);
                                            BigDecimal price = coinArrays.getJSONObject(i).getBigDecimal(PlatformCoinMarket.price);
                                            if (StrKit.notBlank(originalCoinId.trim(), targetCoinId.trim()) && price.compareTo(Constant.zero) == 1) {
                                                sb.append("('").append(IdUtil.nextId()).append("','").append(platform.getPlatformId()).append("','").append(originalCoinId).append("','").append(targetCoinId).append("',").append(price).append(")");
                                                sb.append(j + 1 != size ? "," : "");
                                            }
                                        }
                                        Db.update(sb.toString());
                                        sb.setLength(0);
                                        synResult = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!synResult) {
                        logger.error("{},{}　platform cann't syn coin market info,response json:{}", platform.getPlatformId(), platform.getPlatformName(), jsonResult);
                    }
                }
            });
        }

    }
}
