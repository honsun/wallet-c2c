package com.yaheen.c2c.app.job;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.service.PlatformService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformCoin;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * FileName: SynPlatformCoin
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 每隔2小时自动同步平台币种信息到数据库
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("* */2 * * *")
@EnableDistributedRunnable
public class SynPlatformCoin implements Runnable {

    private Logger logger = LoggerFactory.getLogger(SynPlatformCoin.class);

    @RPCInject
    private PlatformService platformService;

    @Override
    public void run() {
        //从数据库获取最新的平台数据
        List<Platform> platforms = platformService.findAll();
        if (ArrayUtil.isNotEmpty(platforms)) {
            platforms.stream().forEach(platform -> {
                if (StrKit.notBlank(platform.getSynCoinUrl())) {
                    //请求的参数
                    Map<String, String> params = new HashMap<>();
                    params.put(Platform.platformId, platform.getPlatformId());
                    params.put(Platform.accessToken, platform.getAccessToken());

                    boolean synResult = false;
                    StringBuilder sbInsert = new StringBuilder(), sbDelete = new StringBuilder();
                    String jsonResult = "";
                    //尝试5次不成功则报错
                    for (int i = 0; i < 5; i++) {
                        jsonResult = HttpKit.get(platform.getSynCoinUrl(), params);
                        if (StrKit.notBlank(jsonResult)) {
                            JSONObject jsonObject = JSONObject.parseObject(jsonResult);
                            //假设平台返回的data就是我们想要的数组
                            if (null != jsonObject && jsonObject.containsKey("data")) {
                                Object data = jsonObject.get("data");
                                //data必须是数组格式
                                if (data instanceof JSONArray) {
                                    JSONArray coinArrays = (JSONArray) data;
                                    if (!coinArrays.isEmpty() && coinArrays.size() > 0) {
                                        sbInsert.append("insert into tb_platform_coin(`platform_id`,`coin_id`,`coin_name`,`description`) values ");
                                        Set<String> set = new HashSet<>();
                                        for (int j = 0, size = coinArrays.size(); j < size; j++) {
                                            String coinId = coinArrays.getJSONObject(i).getString(PlatformCoin.coinId);
                                            String coinName = coinArrays.getJSONObject(i).getString(PlatformCoin.coinNameDes);
                                            String description = coinArrays.getJSONObject(i).getString(PlatformCoin.description);
                                            if (StrKit.notBlank(coinId.trim(), coinName)) {
                                                set.add(coinId.trim());
                                                sbInsert.append("('").append(platform.getPlatformId()).append("','").append(coinId).append("','").append(coinName).append("','").append(description).append("')");
                                                sbInsert.append(j + 1 != size ? "," : "");
                                            }
                                        }
                                        sbInsert.append(" on duplicate key update `create_time`=now();");
                                        List<String> coinIds = new ArrayList<>(set);
                                        if (ArrayUtil.isNotEmpty(coinIds)) {
                                            //先删除已不存在的货币信息
                                            sbDelete.append("delete ");
                                            sbDelete.append("from tb_platform_coin ");
                                            sbDelete.append("where `platform_id`='").append(platform.getPlatformId()).append("' ");
                                            sbDelete.append("and `coin_id` not in(");
                                            for (int k = 0, size = coinIds.size(); k < size; k++) {
                                                sbDelete.append("'").append(coinIds.get(k)).append("'");
                                                sbDelete.append(k + 1 != size ? "," : "");
                                            }
                                            sbDelete.append(");");
                                            Db.update(sbDelete.toString());
                                            Db.update(sbInsert.toString());
                                        }
                                        sbDelete.setLength(0);
                                        sbInsert.setLength(0);
                                        synResult = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (!synResult) {
                        logger.error("{},{}　platform cann't syn coin info,response json:{}", platform.getPlatformId(), platform.getPlatformName(), jsonResult);
                    }
                }

            });
        }

    }
}
