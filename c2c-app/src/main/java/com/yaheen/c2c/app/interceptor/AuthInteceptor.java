package com.yaheen.c2c.app.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.yaheen.c2c.app.service.PlatformService;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.common.RedisKey;
import com.yaheen.c2c.base.exception.HandlerException;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FileName: AuthInteceptor Author: Honsun Date: 2019/3/26 17:37 Description: 授权过滤器 History:
 * <author> <time> <version> <desc> Honsun 2019/3/26 17:37 2.0.0
 */
public class AuthInteceptor implements Interceptor {

  private Logger logger = LoggerFactory.getLogger(AuthInteceptor.class);

  @RPCInject private UserService userService;

  @RPCInject private PlatformService platformService;

  @Override
  public void intercept(Invocation invocation) {
    Controller controller = invocation.getController();
    // 如果是Ajax首次请求则直接通过
    if (HttpMethods.OPTIONS.equalsIgnoreCase(controller.getRequest().getMethod())) {
      controller.renderJson(Constant.SUCCESS);
    } else {
      // platformId、userId参数
      String platformId = controller.getHeader(User.platformId);
      String userId = controller.getHeader(User.userId);
      if (!StrKit.notBlank(platformId, userId)) {
        throw new HandlerException("header部分参数不能为空");
      }
      platformId = platformId.replaceAll("#", "");
      userId = userId.replaceAll("#", "");
      String redisUserKey = platformId + "-" + userId;
      if (!Jboot.getRedis().exists(platformId)) {
        // PlatformService platformService = Jboot.service(PlatformService.class);
        Platform dbPlatform = platformService.findWithDetail(platformId);
        if (null == dbPlatform) {
          throw new HandlerException("该平台信息不存在，请检查参数");
        } else {
          Jboot.getRedis().set(platformId, dbPlatform);
          Jboot.getRedis().expire(platformId, RedisKey.CACHE_EXPIRE_TIME * 30);
          invocation.invoke();
        }
      } else {
        Jboot.getRedis().expire(redisUserKey, RedisKey.CACHE_EXPIRE_TIME * 30);
        invocation.invoke();
      }

      if (!Jboot.getRedis().exists(redisUserKey)) {
        // UserService userService = Jboot.service(UserService.class);
        User dbUser = userService.findByPlatformIdAndUserId(platformId, userId);
        if (null == dbUser) {
          throw new HandlerException("未登录用户无法访问该资源");
        } else {
          Jboot.getRedis().set(redisUserKey, dbUser);
          Jboot.getRedis().expire(redisUserKey, RedisKey.CACHE_EXPIRE_TIME * 30);
          invocation.invoke();
        }
      } else {
        Jboot.getRedis().expire(redisUserKey, RedisKey.CACHE_EXPIRE_TIME * 30);
        invocation.invoke();
      }
    }
  }
}
