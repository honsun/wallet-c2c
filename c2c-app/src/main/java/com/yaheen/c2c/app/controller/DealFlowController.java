package com.yaheen.c2c.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.PlatformPrepayService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import com.yaheen.c2c.app.service.entity.model.Resource;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;

/**
 * FileName: DealFlowController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 流水管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/dealFlow")
@Api(tags = "交易流水服务")
@Before(AuthInteceptor.class)
public class DealFlowController extends BaseController {

    @RPCInject
    private PlatformPrepayService prepayService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "搜索流水信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = PlatformPrepay.coinId, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = PlatformPrepay.coinIdDes),
            @ApiImplicitParam(name = PlatformPrepay.statusId, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = PlatformPrepay.statusIdDes),
            @ApiImplicitParam(name = Constant.pageNum, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageNumDes, defaultValue = "1"),
            @ApiImplicitParam(name = Constant.pageSize, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageSizeDes, defaultValue = "10")
    })
    @Before({PostRequest.class})
    public void search() throws Exception {
        PlatformPrepay prepay = jsonToModel(PlatformPrepay.class, false, 1);

        String pageNum = prepay.get(Constant.pageNum, "1");
        String pageSize = prepay.get(Constant.pageSize, "10");
        if (!pageNum.matches("[0-9]+") && !pageSize.matches("[0-9]+")) {
            throw new HandlerException("请输入正确的整型");
        }
        prepay.put(User.userId, currentUserId());
        prepay.setPlatformId(currentPlatformId());
        SuccessResult(prepayService.search(prepay, Integer.parseInt(pageNum), Integer.parseInt(pageSize)));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取流水详情",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = PlatformPrepay.platformPrepayId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = PlatformPrepay.platformPrepayIdDes)
    })
    @Before({PostRequest.class})
    public void get() throws Exception {
        PlatformPrepay prepay = jsonToModel(PlatformPrepay.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(prepay.getPlatformPrepayId(), new StringValidator(PlatformPrepay.platformPrepayIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        SuccessResult(prepayService.get(prepay.getPlatformPrepayId()));
    }

}