package com.yaheen.c2c.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.PlatformCoinMarketService;
import com.yaheen.c2c.app.service.entity.model.*;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;

/**
 * FileName: CoinMarketController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 货币行情服务管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/coinMarket")
@Api(tags = "货币行情服务")
@Before(AuthInteceptor.class)
public class CoinMarketController extends BaseController {

    @RPCInject
    private PlatformCoinMarketService platformCoinMarketService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取平台货币行情",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = PlatformCoinMarket.originalCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = PlatformCoinMarket.originalCoinIdDes),
            @ApiImplicitParam(name = PlatformCoinMarket.targetCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = PlatformCoinMarket.targetCoinIdDes)
    })
    @Before({PostRequest.class})
    public void get(User user) {
        PlatformCoinMarket platformCoinMarket = jsonToModel(PlatformCoinMarket.class, false, 1);
        String platformId = currentPlatformId();
        ComplexResult complex = FluentValidator.checkAll()
                .on(platformCoinMarket.getOriginalCoinId(), new StringValidator(HangOrder.originalCoinIdDes))
                .on(platformCoinMarket.getTargetCoinId(), new StringValidator(HangOrder.targetCoinIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        Platform platform = redis.get(platformId);
        if (null == platform) {
            throw new HandlerException("该平台信息不存在");
        }
        if (platform.getReferCoinMarket() == 0) {
            throw new HandlerException("该平台无需参考任何币种行情信息");
        }
        if (!platform.getMarginCoinId().equalsIgnoreCase(platformCoinMarket.getOriginalCoinId()) &&
                !platform.getMarginCoinId().equalsIgnoreCase(platformCoinMarket.getTargetCoinId())) {
            throw new HandlerException("不能查询非交易币种行情信息");
        }
        SuccessResult(platformCoinMarketService.get(platformId, platformCoinMarket.getOriginalCoinId(), platformCoinMarket.getTargetCoinId()));
    }
}