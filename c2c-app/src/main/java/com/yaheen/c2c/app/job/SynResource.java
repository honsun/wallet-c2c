package com.yaheen.c2c.app.job;

import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.service.entity.model.Resource;
import com.yaheen.c2c.base.common.RedisKey;
import io.jboot.Jboot;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import io.jboot.utils.ClassScanner;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Extension;
import io.swagger.annotations.ExtensionProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * FileName: SynData
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 定时更新系统资源到数据库
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/10 * * * *")
@EnableDistributedRunnable
public class SynResource implements Runnable {

    private Logger logger = LoggerFactory.getLogger(SynResource.class);

    @Override
    public void run() {
        List<Class> classes = ClassScanner.scanClassByAnnotation(RequestMapping.class, true);
        if (ArrayUtil.isNotEmpty(classes)) {
            List<Resource> newResources = new ArrayList<>();
            for (Class clazz : classes) {
                RequestMapping requestMapping = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
                String uri = requestMapping.value();
                Method[] methods = clazz.getDeclaredMethods();
                for (Method method : methods) {
                    String fullUri = uri + "/" + method.getName();
                    String operationDes = "";  //注解内容可能为空
                    int joint = 1, needCheck = 1;
                    //读取方法的描述注解
                    Annotation apiOperation = method.getAnnotation(ApiOperation.class);
                    if (null != apiOperation) {
                        operationDes = ((ApiOperation) apiOperation).value();
                        Extension[] extensions = ((ApiOperation) apiOperation).extensions();
                        if (ArrayUtil.isNotEmpty(extensions)) {
                            for (Extension extension : extensions) {
                                for (ExtensionProperty property : extension.properties()) {
                                    if (Resource.joint.equalsIgnoreCase(property.name())) {
                                        joint = "1".equalsIgnoreCase(property.value()) ? 1 : 2;
                                    }
                                    if (Resource.needCheck.equalsIgnoreCase(property.name())) {
                                        needCheck = "1".equalsIgnoreCase(property.value()) ? 1 : 2;
                                    }
                                }
                            }
                        }
                    }
                    Resource resource = new Resource();
                    resource.setUri(fullUri);
                    resource.setDescription(operationDes);
                    resource.setJoint(joint);
                    resource.setNeedCheck(needCheck);
                    newResources.add(resource);
                }
            }
            //如果获取到数据，则需要更新到数据库
            if (ArrayUtil.isNotEmpty(newResources)) {
                //先删除目前系统资源
                Db.update("delete from tb_resource;");
                //再更新到数据库
                Db.batchUpdate(newResources, newResources.size());
                //然后缓存到redis
                Jboot.getRedis().del(RedisKey.CACHE_RESOURCE);
                for (Resource resource : newResources) {
                    Jboot.getRedis().hset(RedisKey.CACHE_RESOURCE, resource.getUri(), resource.getNeedCheck());
                }
            }
        }

    }
}
