package com.yaheen.c2c.app.job;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.service.HangOrderService;
import com.yaheen.c2c.app.service.PlatformPrepayService;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import com.yaheen.c2c.base.common.RedisKey;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import io.jboot.utils.ArrayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * FileName: PrepayFinish
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 平台预支付订单完成后续操作
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/1 * * * *")
@EnableDistributedRunnable
public class PrepayFinish implements Runnable {

    private Logger logger = LoggerFactory.getLogger(PrepayFinish.class);
    private int threads = 4;
    private static boolean startThread = false;

    @RPCInject
    private PlatformPrepayService prepayService;
    @RPCInject
    private HangOrderService hangOrderService;
    @RPCInject
    private UserService userService;

    @Override
    public void run() {
        if (!startThread) {
            startThread = true;
            ExecutorService executor = Executors.newFixedThreadPool(threads);        //创建含4条线程的线程池
            for (int i = 0; i < threads; i++) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        String jsonString = Jboot.getRedis().rpop(RedisKey.CACHE_PREPAY_ID);
                        if (StrKit.notBlank(jsonString)) {
                            JSONObject jsonObject = JSONObject.parseObject(jsonString);
                            String parentPlatformPrepayId = jsonObject.getString(PlatformPrepay.parentPlatformPrepayId);
                            List<PlatformPrepay> dbPrepays = prepayService.findByIdParentPlatformPrepayId(parentPlatformPrepayId);

                            if (ArrayUtil.isNotEmpty(dbPrepays)) {
                                StringBuilder sb = new StringBuilder();
                                String statusCode = jsonObject.getString("statusCode");
                                boolean handleResult = false;

                                int dataSize = dbPrepays.size();
                                if (1 == dataSize) {  //处理保证金和挂单
                                    PlatformPrepay dbPrepay = dbPrepays.get(0);
                                    switch (dbPrepay.getType()) {
                                        case 1:  //缴纳保证金，此时list只有一条数据
                                            if ("200".equalsIgnoreCase(statusCode)) {
                                                //成功缴纳保证金，则需要更新预支付订单状态和更新user表数据即可
                                                dbPrepay.setStatusId(2);
                                                sb.append("update tb_user set `margin`=? where `platform_id`=? and `user_id`=?");
                                                handleResult = dbPrepay.update() && Db.update(sb.toString(), dbPrepay.getQuantity(), dbPrepay.getPlatformId(), dbPrepay.getPayerId()) > 0;
                                            } else {
                                                //缴纳保证金失败，则需要更新预支付订单状态
                                                dbPrepay.setStatusId(3);
                                                sb.append("update tb_user set `margin`=0 where `platform_id`=? and `user_id`=?");
                                                handleResult = dbPrepay.update() && Db.update(sb.toString(), dbPrepay.getPlatformId(), dbPrepay.getPayerId()) > 0;
                                            }
                                            if (!handleResult) {
                                                logger.error("prepayId:{},dbPrepay:{} 处理发生错误", parentPlatformPrepayId, dbPrepay.toJson());
                                                //处理失败后把数据扔回去redis
                                                Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, jsonString);
                                            } else {
                                                //需要更新redis上user的信息
                                                String redisKey = dbPrepay.getPlatformId() + "-" + dbPrepay.getPayerId();
                                                Jboot.getRedis().set(redisKey, userService.findByPlatformIdAndUserId(dbPrepay.getPlatformId(), dbPrepay.getPayerId()));
                                                //Jboot.getRedis().expire(redisKey, RedisKey.CACHE_EXPIRE_TIME * 30);
                                            }
                                            sb.setLength(0);
                                            break;
                                        case 2:  //退款保证金，此时list只有一条数据
                                            if ("200".equalsIgnoreCase(statusCode)) {
                                                //成功退款保证金，则需要更新预支付订单状态和更新user表数据即可
                                                dbPrepay.setStatusId(2);
                                                sb.append("update tb_user set `margin`=0 where `platform_id`=? and `user_id`=?");
                                                handleResult = dbPrepay.update() && Db.update(sb.toString(), dbPrepay.getQuantity(), dbPrepay.getPlatformId(), dbPrepay.getPayerId()) > 0;
                                            } else {
                                                //退款保证金失败，则需要更新预支付订单状态
                                                dbPrepay.setStatusId(3);
                                                handleResult = dbPrepay.update();
                                            }
                                            if (!handleResult) {
                                                logger.error("prepayId:{},dbPrepay:{} 处理发生错误", parentPlatformPrepayId, dbPrepay.toJson());
                                                //处理失败后把数据扔回去redis
                                                Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, jsonString);
                                            } else {
                                                //需要更新redis上user的信息
                                                String redisKey = dbPrepay.getPlatformId() + "-" + dbPrepay.getPayerId();
                                                Jboot.getRedis().set(redisKey, userService.findByPlatformIdAndUserId(dbPrepay.getPlatformId(), dbPrepay.getPayerId()));
                                                //Jboot.getRedis().expire(redisKey, RedisKey.CACHE_EXPIRE_TIME * 30);
                                            }
                                            sb.setLength(0);
                                            break;
                                        case 3:  //锁仓挂单，此时list只有一条数据
                                            if ("200".equalsIgnoreCase(statusCode)) {
                                                //成功锁仓则需要更挂单状态即可
                                                sb.append("update tb_hang_order set `status_id`=1 where `platform_prepay_id`=?");
                                                handleResult = Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            } else {
                                                //锁仓失败则需要删除预支付订单及挂单信息
                                                sb.append("delete from tb_hang_order where `platform_prepay_id`=?");
                                                handleResult = dbPrepay.delete() && Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            }
                                            if (!handleResult) {
                                                logger.error("prepayId:{},dbPrepay:{} 处理发生错误", parentPlatformPrepayId, dbPrepay.toJson());
                                                //处理失败后把数据扔回去redis
                                                Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, jsonString);
                                            }
                                            sb.setLength(0);
                                            break;
                                        case 4:  //取消挂单，此时list只有一条数据
                                            if ("200".equalsIgnoreCase(statusCode)) {
                                                //成功退款则需要更新挂单状态即可
                                                sb.append("update tb_hang_order set `status_id`=5,`cancel_time`=now() where `platform_prepay_id`=?");
                                                handleResult = Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            } else {
                                                //退款失败则需要将挂单的退款预支付订单设为空
                                                sb.append("update tb_hang_order set `refund_platform_prepay_id`='' where `platform_prepay_id`=?");
                                                handleResult = dbPrepay.delete() && Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            }
                                            if (!handleResult) {
                                                logger.error("prepayId:{},dbPrepay:{} 处理发生错误", parentPlatformPrepayId, dbPrepay.toJson());
                                                //处理失败后把数据扔回去redis
                                                Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, jsonString);
                                            }
                                            sb.setLength(0);
                                            break;
                                        case 5:  //系统自动关闭挂单，此时list只有一条数据
                                            if ("200".equalsIgnoreCase(statusCode)) {
                                                //成功退款则需要更新挂单状态即可
                                                sb.append("update tb_hang_order set `status_id`=4,`finish_time`=now() where `platform_prepay_id`=?");
                                                handleResult = Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            } else {
                                                //退款失败则需要将挂单的退款预支付订单设为空
                                                sb.append("update tb_hang_order set `refund_platform_prepay_id`='' where `platform_prepay_id`=?");
                                                handleResult = dbPrepay.delete() && Db.update(sb.toString(), dbPrepay.getPlatformPrepayId()) > 0;
                                            }
                                            if (!handleResult) {
                                                logger.error("prepayId:{},dbPrepay:{} 处理发生错误", parentPlatformPrepayId, dbPrepay.toJson());
                                                //处理失败后把数据扔回去redis
                                                Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, jsonString);
                                            }
                                            sb.setLength(0);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                if (5 == dataSize) {  //处理订单
                                    List<String> prepayIds = new ArrayList<>();
                                    for (PlatformPrepay dbPrepay : dbPrepays) {
                                        dbPrepay.setStatusId("200".equalsIgnoreCase(statusCode) ? 2 : 3);
                                        prepayIds.add(dbPrepay.getPlatformPrepayId());
                                    }
                                    Db.batchUpdate(dbPrepays, dataSize);

                                    sb.setLength(0);
                                    sb.append("update tb_order set `status_id`=").append("200".equalsIgnoreCase(statusCode) ? 2 : 3);
                                    sb.append(",finish_time=now() ");
                                    sb.append("where `platform_prepay_id` in(");
                                    for (int j = 0, size = prepayIds.size(); j < size; j++) {
                                        sb.append("'").append(prepayIds.get(j)).append("'");
                                        sb.append(j + 1 != size ? "" : ",");
                                    }
                                    sb.append(");");
                                    Db.update(sb.toString());
                                    sb.setLength(0);
                                    if (!"200".equalsIgnoreCase(statusCode)) {
                                        //返回订单的可购买数量到挂单上
                                        sb.append("update tb_hang_order ho,tb_order o ");
                                        sb.append("set ho.`remain`=ho.`remain`+o.`target_coin_id`*(o.`target_coin_quantity`+o.`total_charge`) ");
                                        sb.append("where ho.`hang_order_id`=o.`hang_order_id` ");
                                        sb.append("and ho.`original_coin_id`=o.`target_coin_id` and ho.`target_coin_id`=o.`original_coin_id` ");
                                        sb.append("and o.`platform_prepay_id` in(");
                                        for (int j = 0, size = prepayIds.size(); j < size; j++) {
                                            sb.append("'").append(prepayIds.get(j)).append("'");
                                            sb.append(j + 1 != size ? "" : ",");
                                        }
                                        sb.append(");");
                                        Db.update(sb.toString());
                                    }
                                }
                            }
                        }


                    }
                });
            }
        }
    }

}
