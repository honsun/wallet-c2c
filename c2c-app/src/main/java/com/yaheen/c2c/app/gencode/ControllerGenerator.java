package com.yaheen.c2c.app.gencode;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import com.yaheen.c2c.base.gencode.ScanTable;
import com.yaheen.c2c.base.gencode.controller.AppControllerGeneratorConfig;
import io.jboot.Jboot;
import io.jboot.utils.ArrayUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * FileName: ControllerGenerator
 * Author:   Honsun
 * Date:     2019/4/2 11:20
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/4/2 11:20     2.0.0
 */
public class ControllerGenerator {

    public static void main(String[] args) throws IOException {
        System.out.println("start generate controller class...");
        AppControllerGeneratorConfig config = Jboot.config(AppControllerGeneratorConfig.class);
        String controllerPackage = config.getControllerpackage();
        String controllerTemplate = config.getControllertemplate();
        //项目名称
        String appName = controllerPackage.substring(controllerPackage.lastIndexOf("c2c.") + 4, controllerPackage.lastIndexOf("."));
        Engine engine = Engine.create("myEngine");
        engine.setBaseTemplatePath(PathKit.getWebRootPath() + "/src/main/java/");
        engine.setDevMode(true);
        engine.addSharedMethod(new StrKit());

        //查询数据库所有表信息
        List<String> tableNames = ScanTable.getTableNames();
        if (ArrayUtil.isNotEmpty(tableNames)) {
            StringBuilder sb = new StringBuilder();
            for (String tableName : tableNames) {
                tableName = tableName.replaceAll("tb_", "");
                String[] names = tableName.split("_");
                for (String name : names) {
                    sb.append(StrKit.firstCharToUpperCase(name));
                }
                String className = sb.toString();
                Map<String, String> record = getInfo(controllerPackage, appName, className);
                Template template = engine.getTemplate(controllerTemplate);
                String dataController = template.renderToString(record);
                writeToFile(record, dataController);
                sb.setLength(0);
            }
        }
        System.out.println("controller class generate finished !!!");
    }

    /***
     * controller
     * @param className 类名称
     * @return 返回封装好的Map信息
     */
    private static Map<String, String> getInfo(String controllerPackage, String appName, String className) {
        Map<String, String> record = new HashMap<>(4);
        record.put("packageName", controllerPackage);
        record.put("appName", appName);
        record.put("className", className);
        String filePath = PathKit.getWebRootPath() + "/src/main/java/" + controllerPackage;
        filePath = filePath.replace(".", "/");
        String fileName = "/" + className + "Controller.java";
        record.put("fileName", fileName);
        record.put("filePath", filePath);
        return record;
    }

    /**
     * 内容写入到文件总
     */
    private static void writeToFile(Map<String, String> record, String data) throws IOException {
        String fileName = record.get("fileName");
        String filePath = record.get("filePath");
        File dir = new File(filePath);
        if (!dir.exists()) {
            boolean res = dir.mkdirs();
            if (!res) {
                throw new RuntimeException("文件夹创建失败，无法创建模板文件，请重试");
            }
        }
        File file = new File(filePath, fileName);
        //当前文件不存在的时候才写入，否则会覆盖文件
        if (!file.exists()) {
            try (OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(filePath + fileName), StandardCharsets.UTF_8)) {
                osw.write(data);
            }
        }
    }
}