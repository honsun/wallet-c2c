package com.yaheen.c2c.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.*;
import com.yaheen.c2c.app.service.entity.model.*;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.Constant;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * FileName: OrderController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 订单服务管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/order")
@Api(tags = "订单服务")
@Before(AuthInteceptor.class)
public class OrderController extends BaseController {

    @RPCInject
    private UserService userService;
    @RPCInject
    private OrderService orderService;
    @RPCInject
    private HangOrderService hangOrderService;
    @RPCInject
    private PlatformCoinMarketService platformCoinMarketService;
    @RPCInject
    private PlatformHangorderCoinTypeService hangorderCoinTypeService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取订单详情",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = Order.orderId, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = Order.orderIdDes),
    })
    @Before({PostRequest.class})
    public void get() throws Exception {
        Order order = jsonToModel(Order.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(order.getOrderId(), new StringValidator(Order.orderIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        SuccessResult(orderService.get(order.getOrderId(), currentPlatformId()));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "搜索订单信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = PlatformCoin.coinId, required = false, paramType = ParamType.BODY, dataType = DataType.STRING, value = PlatformCoin.coinIdDes),
            @ApiImplicitParam(name = Order.statusId, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.statusIdDes),
            @ApiImplicitParam(name = Constant.pageNum, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageNumDes, defaultValue = "1"),
            @ApiImplicitParam(name = Constant.pageSize, required = false, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Constant.pageSizeDes, defaultValue = "10"),
    })
    @Before({PostRequest.class})
    public void search() throws Exception {
        Order order = jsonToModel(Order.class, false, 1);
        String status = order.get(Order.statusId, "0");
        String pageNum = order.get(Constant.pageNum, "1");
        String pageSize = order.get(Constant.pageSize, "10");
        if (!status.matches("[0-9]+") && !pageNum.matches("[0-9]+") && !pageSize.matches("[0-9]+")) {
            throw new HandlerException("请输入正确的整型");
        }
        SuccessResult(orderService.search(currentUserId(), order.getStr(PlatformCoin.coinId), Integer.parseInt(status), Integer.parseInt(pageNum), Integer.parseInt(pageSize)));
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "添加订单信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = Order.hangOrderId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.hangOrderIdDes),
            @ApiImplicitParam(name = Order.originalCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.originalCoinIdDes),
            @ApiImplicitParam(name = Order.targetCoinId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.targetCoinId),
            @ApiImplicitParam(name = Order.targetCoinQuantity, required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = Order.targetCoinQuantityDes),
    })
    @Before({PostRequest.class, AuthInteceptor.class})
    public void add() throws Exception {
        Order order = jsonToModel(Order.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(order.getHangOrderId(), new StringValidator(Order.hangOrderIdDes))
                .on(order.getOriginalCoinId(), new StringValidator(Order.originalCoinIdDes))
                .on(order.getTargetCoinId(), new StringValidator(Order.targetCoinId))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        String platformId = currentPlatformId();
        Platform platform = redis.get(platformId);
        if (!hangorderCoinTypeService.exist(platform.getPlatformId(), order.getTargetCoinId(), order.getOriginalCoinId())) {
            throw new HandlerException("非法的币币交易对");
        }
        if (!StrKit.notBlank(order.getStr(Order.targetCoinQuantity))) {
            throw new HandlerException("目标币种参数错误");
        }
        HangOrder hangOrder = hangOrderService.findById(order.getHangOrderId());
        if (null == hangOrder) {
            throw new HandlerException("该挂单信息不存在");
        }
        if (2 != hangOrder.getStatusId()) {
            throw new HandlerException("成功锁仓的挂单才能交易");
        }
        BigDecimal targetCoinQuantity = new BigDecimal(order.getStr(Order.targetCoinQuantity)).setScale(6, BigDecimal.ROUND_DOWN);
        if (0 == platform.getSplitHangorder()) {
            if (0 != targetCoinQuantity.compareTo(hangOrder.getMinBuy())) {
                throw new HandlerException("购买数量必须等于挂单可购买数量");
            }
        } else {
            if (-1 == targetCoinQuantity.compareTo(hangOrder.getMinBuy()) || 1 == targetCoinQuantity.compareTo(hangOrder.getMaxBuy())) {
                throw new HandlerException("数量必须在最少购买数与最大购买数之间");
            }
            if (1 != hangOrder.getRemain().compareTo(Constant.zero) || 1 != hangOrder.getRemain().compareTo(hangOrder.getMinBuy())) {
                throw new HandlerException("购买数量不能少于挂单剩余可购买数量");
            }
        }
        order.setUserId(currentUserId());
        order.setOriginalCoinQuantity(targetCoinQuantity.multiply(hangOrder.getPrice()));
        order.setTargetCoinQuantity(targetCoinQuantity);
        if (!orderService.add(platform, hangOrder, order)) {
            throw new HandlerException("生成订单过程发生错误,请重试");
        }
        SuccessResult();
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "取消订单", notes = "挂单用户不能取消订单",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521"),

            @ApiImplicitParam(name = Order.hangOrderId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.hangOrderIdDes),
            @ApiImplicitParam(name = Order.orderId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = Order.orderIdDes),
    })
    @Before({PostRequest.class})
    public void cancel() throws Exception {
        Order order = jsonToModel(Order.class, false, 1);
        ComplexResult complex = FluentValidator.checkAll()
                .on(order.getHangOrderId(), new StringValidator(Order.hangOrderIdDes))
                .on(order.getOrderId(), new StringValidator(Order.orderIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        HangOrder dbHangOrder = hangOrderService.findById(order.getHangOrderId());
        if (null == dbHangOrder) {
            throw new HandlerException("该挂单信息不存在");
        }
        Order dbOrder = orderService.findById(order.getOrderId());
        if (null == dbOrder) {
            throw new HandlerException("该订单信息不存在");
        }
        if (!dbOrder.getHangOrderId().equalsIgnoreCase(dbHangOrder.getHangOrderId())) {
            throw new HandlerException("该挂单下没有此订单信息");
        }
        if (dbOrder.getHangOrderId().equalsIgnoreCase(dbHangOrder.getUserId())) {
            throw new HandlerException("挂单用户不能取消订单信息");
        }
        if (dbOrder.getStatusId() != 1) {
            throw new HandlerException("只有未付款的订单才能取消");
        }
        /**
         * 为了避免平台预支付成功但回调处理未完成导致的问题
         * 这里需要平台实时检测该预支付订单是否支付成功
         */
        Platform platform = redis.get(dbHangOrder.getPlatformId());
        //请求平台查询该预支付订单的支付状态
        Map<String, String> params = new HashMap<>();
        //平台参数
        params.put(Platform.platformId, platform.getPlatformId());
        params.put(Platform.accessToken, platform.getAccessToken());
        params.put(PlatformPrepay.parentPlatformPrepayId, dbOrder.getPlatformPrepayId());
//        String jsonResult = HttpKit.post(platform.getPrepayCheckUrl(), params, null);
//
//        if (StrKit.isBlank(jsonResult)) {
//            throw new HandlerException("暂时无法获取该订单支付状态，请稍候再试");
//        }
//        JSONObject jsonObject = JSONObject.parseObject(jsonResult);
//        if (null == jsonObject || !jsonObject.containsKey("code") || 200 == jsonObject.getIntValue("code") || jsonObject.containsKey("data")) {
//            throw new HandlerException("暂时无法获取该订单支付状态，请稍候再试");
//        }
//        if (!(jsonObject.get("data") instanceof JSONObject)) {
//            throw new HandlerException("接口返回数据错误，请联系管理员");
//        }
//        JSONObject dataObject = jsonObject.getJSONObject("data");
//        if ("200".equalsIgnoreCase(dataObject.getString("statusCode"))) {
//            //证明平台那边已经支付成功，这边不允许取消订单
//            throw new HandlerException("该订单已经付款成功，无法取消");
//        }
        boolean cancelResult = false;
        //取消订单操作跟支付失败操作一样
        StringBuilder sb = new StringBuilder();
        sb.append("update tb_platform_prepay set `status_id`=4 where parent_platform_prepay_id=?");
        cancelResult = Db.update(sb.toString(), dbOrder.getPlatformPrepayId()) > 0;
        if (!cancelResult) {
            throw new HandlerException("取消订单失败，请重试");
        }
        sb.setLength(0);
        sb.append("update tb_order set `status_id`=4,cancel_time=now() where platform_prepay_id=?");
        cancelResult = Db.update(sb.toString(), dbOrder.getPlatformPrepayId()) > 0;
        if (!cancelResult) {
            throw new HandlerException("取消订单失败，请重试");
        }
        sb.setLength(0);
        //返回订单的可购买数量到挂单上
        sb.append("update tb_hang_order ho,tb_order o ");
        sb.append("set ho.`remain`=ho.`remain`+o.target_coin_quantity+total_charge ");
        sb.append("where ho.hang_order_id=o.hang_order_id ");
        sb.append("and ho.original_coin_id=o.target_coin_id and ho.target_coin_id=o.original_coin_id ");
        sb.append("and o.platform_prepay_id=? ");
        cancelResult = Db.update(sb.toString(), dbOrder.getPlatformPrepayId()) > 0;
        if (!cancelResult) {
            throw new HandlerException("取消订单失败，请重试");
        }
        SuccessResult("成功取消该订单");
    }

}