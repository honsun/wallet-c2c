package com.yaheen.c2c.app.controller;

import com.baidu.unbiz.fluentvalidator.ComplexResult;
import com.baidu.unbiz.fluentvalidator.FluentValidator;
import com.baidu.unbiz.fluentvalidator.ResultCollectors;
import com.jfinal.aop.Before;
import com.yaheen.c2c.app.service.PlatformPrepayService;
import com.yaheen.c2c.app.service.UserService;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.PlatformPrepay;
import com.yaheen.c2c.app.service.entity.model.Resource;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.common.RedisKey;
import com.yaheen.c2c.base.exception.HandlerException;
import com.yaheen.c2c.base.interceptor.PostRequest;
import com.yaheen.c2c.base.validator.StringValidator;
import io.jboot.Jboot;
import io.jboot.components.rpc.annotation.RPCInject;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.utils.ArrayUtil;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;

import java.util.List;

/**
 * FileName: ServiceController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 对接平台接口服务
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */
@RequestMapping("/c2c/service")
@Api(tags = "对接服务-第三方对接使用")
public class ServiceController extends BaseController {

    @RPCInject
    private PlatformPrepayService prepayService;
    @RPCInject
    private UserService userService;

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "注册用户信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "2")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "2")
                    })}
                    ,consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.platformId),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = User.userIdDes),
    })
    @Before({PostRequest.class})
    public void registerUser() throws Exception {
        User user = jsonToModel(User.class, false, 2);
        ComplexResult complex = FluentValidator.checkAll()
                .on(user.getPlatformId(), new StringValidator(User.platformIdDes))
                .on(user.getUserId(), new StringValidator(User.userIdDes))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        //平台ID存在代表我们已对接该平台
        if (!redis.exists(user.getPlatformId())) {
            throw new HandlerException("该平台ID不存在");
        }
        if (!userService.add(user.getPlatformId(), user.getUserId())) {
            throw new HandlerException("注册失败，请重试");
        }
        SuccessResult();
    }

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "支付完成后回调", notes = "一个预支付订单只接收一次回调处理",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "2")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "2")
                    })},consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = PlatformPrepay.platformId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = PlatformPrepay.platformIdDes),
            @ApiImplicitParam(name = PlatformPrepay.parentPlatformPrepayId, required = true, paramType = ParamType.BODY, dataType = DataType.INTEGER, value = PlatformPrepay.parentPlatformPrepayIdDes),
            @ApiImplicitParam(name = "statusCode", required = true, paramType = ParamType.BODY, dataType = DataType.STRING, value = "支付状态码(200成功 其他:失败)"),
    })
    @Before({PostRequest.class})
    public void prepayFinish() throws Exception {
        PlatformPrepay prepay = jsonToModel(PlatformPrepay.class, false, 2);
        ComplexResult complex = FluentValidator.checkAll()
                .on(prepay.getPlatformId(), new StringValidator(PlatformPrepay.platformIdDes))
                .on(prepay.getParentPlatformPrepayId(), new StringValidator(PlatformPrepay.parentPlatformPrepayIdDes))
                .on(prepay.getStr("statusCode"), new StringValidator("支付状态码(200成功 其他:失败)"))
                .doValidate().result(ResultCollectors.toComplex());
        validation(complex);
        //平台ID存在代表我们已对接该平台
        if (!redis.exists(prepay.getPlatformId())) {
            throw new HandlerException("该平台ID不存在");
        }
        List<PlatformPrepay> dbPrepays = prepayService.findByIdParentPlatformPrepayId(prepay.getParentPlatformPrepayId());
        if (ArrayUtil.isNullOrEmpty(dbPrepays)) {
            throw new HandlerException("该预支付订单信息不存在");
        }
        //防止平台重复调用
        for (PlatformPrepay dbPrepay : dbPrepays) {
            if (2 == dbPrepay.getStatusId()) {
                throw new HandlerException("该预支付订单的状态是已付款");
            }
            if (4 == dbPrepay.getStatusId()) {
                throw new HandlerException("该预支付订单的状态是已取消");
            }
        }
        //保存到redis用于异步多线程处理
        Jboot.getRedis().lpush(RedisKey.CACHE_PREPAY_ID, prepay.toJson());
        SuccessResult("成功接收该预支付订单数据");
    }

}