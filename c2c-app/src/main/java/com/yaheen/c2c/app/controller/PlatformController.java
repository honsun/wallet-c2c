package com.yaheen.c2c.app.controller;

import com.jfinal.aop.Before;
import com.yaheen.c2c.app.interceptor.AuthInteceptor;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.Resource;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.BaseController;
import com.yaheen.c2c.base.common.DataType;
import com.yaheen.c2c.base.common.HttpMethods;
import com.yaheen.c2c.base.interceptor.PostRequest;
import io.jboot.support.metric.annotation.EnableMetricConcurrency;
import io.jboot.support.metric.annotation.EnableMetricCounter;
import io.jboot.support.swagger.ParamType;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.*;

/**
 * FileName: PlatformCoinController
 * Author:   Honsun
 * Date:     2019/3/18 17:12
 * Description: 平台货币服务管理
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/25 17:12     2.0.0
 */

@RequestMapping("/c2c/platform")
@Api(tags = "平台服务")
@Before(AuthInteceptor.class)
public class PlatformController extends BaseController {

    @EnableMetricCounter
    @EnableMetricConcurrency
    @ApiOperation(httpMethod = HttpMethods.POST, value = "获取平台信息",
            extensions = {
                    @Extension(name = Resource.joint, properties = {@ExtensionProperty(name = Resource.joint, value = "1")}),
                    @Extension(name = Resource.needCheck, properties = {@ExtensionProperty(name = Resource.needCheck, value = "1")
                    })},
            consumes = "application/json"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = User.platformId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.platformIdDes, defaultValue = "566203807701139456"),
            @ApiImplicitParam(name = User.userId, required = true, paramType = ParamType.HEADER, dataType = DataType.STRING, value = User.userIdDes, defaultValue = "566214299505131521")})
    @Before({PostRequest.class})
    public void get() {
        Platform platform = redis.get(currentPlatformId());
        platform.remove(Platform.accessToken, Platform.totalCharge, Platform.platformCharge, Platform.c2cCharge, Platform.marginCoinId, Platform.marginQuantity);
        platform.remove(Platform.synCoinUrl, Platform.synCoinMarketUrl, Platform.getUserinfoUrl, Platform.prepayUrl, Platform.prepayCheckUrl);
        platform.remove("managedAccount", "chargeAccount");
        SuccessResult(platform);
    }

}
