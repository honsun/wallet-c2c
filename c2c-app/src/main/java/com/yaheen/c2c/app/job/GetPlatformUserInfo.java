package com.yaheen.c2c.app.job;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.yaheen.c2c.app.service.entity.model.Platform;
import com.yaheen.c2c.app.service.entity.model.User;
import com.yaheen.c2c.base.common.RedisKey;
import com.yaheen.c2c.base.util.StringUtil;
import io.jboot.Jboot;
import io.jboot.components.schedule.annotation.Cron;
import io.jboot.components.schedule.annotation.EnableDistributedRunnable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * FileName: GetPlatformUserInfo
 * Author:   Honsun
 * Date:     2019/3/22 17:38
 * Description: 每隔1分钟自动启动同步线程
 * History:
 * <author>          <time>          <version>          <desc>
 * Honsun         2019/3/22 17:38     2.0.0
 */
@Cron("*/1 * * * *")
@EnableDistributedRunnable
public class GetPlatformUserInfo implements Runnable {

    private Logger logger = LoggerFactory.getLogger(GetPlatformUserInfo.class);
    private int threads = 4;

    private static boolean startThread = false;

    @Override
    public void run() {
        if (!startThread) {
            startThread = true;
            ExecutorService executor = Executors.newFixedThreadPool(threads);        //创建含4条线程的线程池
            for (int i = 0; i < threads; i++) {
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        String userJson = Jboot.getRedis().rpop(RedisKey.CACHE_GET_USERINFO);
                        if (StrKit.notBlank(userJson)) {
                            User user = StringUtil.jsonToObject(userJson, User.class);
                            if (null == user) {
                                logger.error("user info:{} error", userJson);
                            }
                            Platform platform = Jboot.getRedis().get(user.getPlatformId());
                            if (null == platform) {
                                logger.error("user platformId:{} error", user.getPlatformId());
                            }
                            String getUserInfoUrl = platform.getGetUserinfoUrl();
                            if (StrKit.isBlank(getUserInfoUrl)) {
                                logger.error("platform get userinfo url is empty", user.getPlatformId());
                                //放回去redis继续等待处理
                                Jboot.getRedis().lpush(RedisKey.CACHE_GET_USERINFO, userJson);
                            }
                            //请求的参数
                            Map<String, String> params = new HashMap<>();
                            params.put(Platform.platformId, platform.getPlatformId());
                            params.put(Platform.accessToken, platform.getAccessToken());
                            params.put(User.userId, user.getUserId());
                            params.put(User.accessToken, user.getAccessToken());
                            String responseJson = HttpKit.get(getUserInfoUrl, params);
                            if (StrKit.isBlank(responseJson)) {
                                logger.error("platform get userinfo[url:{},params:{}] response is empty", getUserInfoUrl, JsonKit.toJson(params));
                            }
                            JSONObject jsonObject = JSONObject.parseObject(responseJson);
                            //假设平台返回的data就是我们想要的数组
                            if (null != jsonObject && jsonObject.containsKey("data")) {
                                Object data = jsonObject.get("data");
                                //data必须是对象格式
                                if (data instanceof JSONObject) {
                                    JSONObject userObject = (JSONObject) data;
                                    String nickname = userObject.getString(User.nickname);
                                    String realname = userObject.getString(User.realname);
                                    String headimg = userObject.getString(User.headimg);
                                    String email = userObject.getString(User.email);
                                    Db.update("update tb_user set `nickname`=?,`realname`=?,`headimg`=?,`email`=? where `platform_id`=? and `user_id`=?",
                                            nickname, realname, headimg, email, platform.getPlatformId(), user.getUserId());
                                    Jboot.getRedis().setex(platform.getPlatformId() + "-" + user.getUserId(), RedisKey.CACHE_EXPIRE_TIME * 30, "1");
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
