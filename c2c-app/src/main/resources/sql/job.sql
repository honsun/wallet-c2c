#sql("findTimeoutOrders")
  select o.*
  from tb_hang_order ho
  left join tb_order ho on o.hang_order_id=ho.hang_order_id and o.user_id<>ho.user_id
  where o.`status_id`=1 and timestampdiff(MINUTE,o.`start_time`,now())>=30
#end

#sql("findTimeoutHangOrders")
  select ho.*
  from tb_hang_order ho
  left join tb_platform_prepay pp on pp.platform_prepay_id=ho.parent_platform_prepay_id
  where ho.status_id=1 and timestampdiff(MINUTE,ho.`start_time`,now())>=30 and pp.status_id<>2
#end

#sql("findBelowStandardHangOrders")
  select ho.*
  from tb_hang_order ho
  where (ho.`status_id`=2 or ho.`status_id`=4) and ho.`remain` < ho.`min_buy`
  and length(ho.`refund_platform_prepay_id`)=0;
#end